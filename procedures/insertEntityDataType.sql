/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.insertEntityDataType;
DELIMITER //

/*
 * Insert the (default) datatype of a property.
 *
 * Parameters
 * ----------
 * PropertyID : VARCHAR(255)
 *   The property id.
 * DataTypeID : VARCHAR(255)
 *   The data type id (not the name!)
 */
CREATE PROCEDURE db_5_0.insertEntityDataType(in PropertyID VARCHAR(255), in DataTypeID VARCHAR(255))
BEGIN
    DECLARE InternalPropertyID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalPropertyID FROM entity_ids WHERE id=PropertyID;

    INSERT INTO data_type (domain_id, entity_id, property_id, datatype) SELECT 0, 0, InternalPropertyID, ( SELECT internal_id FROM entity_ids WHERE id = DataTypeID);


END;
//
DELIMITER ;


DROP PROCEDURE IF EXISTS db_5_0.insertEntityCollection;
DELIMITER //

/*
 * Insert the (default) collection type of a property.
 *
 * Parameters
 * ----------
 * PropertyID : VARCHAR(255)
 *   The property id.
 * Collection : VARCHAR(255)
 *   The collection, e.g. "LIST"
 */
CREATE PROCEDURE db_5_0.insertEntityCollection(in PropertyID VARCHAR(255), in Collection VARCHAR(255))
BEGIN
    DECLARE InternalPropertyID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalPropertyID FROM entity_ids WHERE id=PropertyID;

    INSERT INTO collection_type (domain_id, entity_id, property_id, collection) SELECT 0, 0, InternalPropertyID, Collection;

END;
//
DELIMITER ;
