/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.overrideName;
DROP PROCEDURE IF EXISTS db_5_0.overrideDesc;
DROP PROCEDURE IF EXISTS db_5_0.overrideType;

DELIMITER //
/*
 * Insert a name override.
 *
 * Parameters
 * ----------
 * InternalDomainID : INT UNSIGNED
 *     The *internal* id of the domain.
 * InternalEntityID : INT UNSIGNED
 *     The *internal* id of the entity.
 * InternalPropertyID : INT UNSIGNED
 *     The *internal* id of the property.
 * Name : VARCHAR(255)
 */
CREATE PROCEDURE db_5_0.overrideName(in InternalDomainID INT UNSIGNED, in InternalEntityID INT UNSIGNED, in InternalPropertyID INT UNSIGNED, in Name VARCHAR(255))
BEGIN
    INSERT INTO name_overrides (domain_id, entity_id, property_id, name) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, Name);
END;
//

/*
 * Insert a description override.
 *
 * Parameters
 * ----------
 * InternalDomainID : INT UNSIGNED
 *     The *internal* id of the domain.
 * InternalEntityID : INT UNSIGNED
 *     The *internal* id of the entity.
 * InternalPropertyID : INT UNSIGNED
 *     The *internal* id of the property.
 * Description : TEXT
 */
CREATE PROCEDURE db_5_0.overrideDesc(in InternalDomainID INT UNSIGNED, in InternalEntityID INT UNSIGNED, in InternalPropertyID INT UNSIGNED, in Description TEXT)
BEGIN
    INSERT INTO desc_overrides (domain_id, entity_id, property_id, description) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, Description);
END;
//

/*
 * Insert a data type override.
 *
 * Parameters
 * ----------
 * InternalDomainID : INT UNSIGNED
 *     The *internal* id of the domain.
 * InternalEntityID : INT UNSIGNED
 *     The *internal* id of the entity.
 * InternalPropertyID : INT UNSIGNED
 *     The *internal* id of the property.
 * InternalDatatypeID : INT UNSIGNED
 *     The *internal* id of the data type.
 */
CREATE PROCEDURE db_5_0.overrideType(in InternalDomainID INT UNSIGNED, in InternalEntityID INT UNSIGNED, in InternalPropertyID INT UNSIGNED, in InternalDataTypeID INT UNSIGNED)
BEGIN
    INSERT INTO data_type (domain_id, entity_id, property_id, datatype) VALUES (InternalDomainID, InternalEntityID, InternalPropertyID, InternalDataTypeID);
END;
//

DELIMITER ;
