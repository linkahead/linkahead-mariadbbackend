/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


DROP PROCEDURE IF EXISTS db_5_0.isSubtype;
delimiter //

/*
 * Return TRUE if the given Child is indeed a (direct or indirect) child of the
 * given Parent.
 *
 * Parameters
 * ----------
 * ChildID : VARCHAR(255)
 *     The entity id of the child.
 * ParentID : VARCHAR(255)
 *     The entity id of the parent.
 *
 * Returns
 * -------
 * ISA : BOOLEAN
 */
CREATE PROCEDURE db_5_0.isSubtype(in ChildID VARCHAR(255), in ParentID VARCHAR(255))
BEGIN
    DECLARE c INT UNSIGNED DEFAULT NULL;
    DECLARE p INT UNSIGNED DEFAULT NULL;
    DECLARE ret BOOLEAN DEFAULT FALSE;

    SELECT internal_id INTO c from entity_ids WHERE id = ChildID;
    SELECT internal_id INTO p from entity_ids WHERE id = ParentID;

    SELECT TRUE INTO ret FROM isa_cache AS i WHERE i.child=c AND i.parent=p LIMIT 1;
    SELECT ret as ISA;
END;
//
delimiter ;
