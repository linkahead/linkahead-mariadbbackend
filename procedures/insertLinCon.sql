/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_5_0.insertLinCon;
delimiter //

CREATE PROCEDURE db_5_0.insertLinCon(in signature_from BIGINT, in signature_to BIGINT, in a DECIMAL(65,30), in b_dividend BIGINT, in b_divisor BIGINT, in c DECIMAL(65,30))
BEGIN

    INSERT IGNORE INTO units_lin_con (signature_from, signature_to, a, b_dividend, b_divisor, c) VALUES (signature_from, signature_to, a, b_dividend, b_divisor, c);

END;
//
delimiter ;

DROP PROCEDURE IF EXISTS db_5_0.deleteLinCon;
delimiter //

CREATE PROCEDURE db_5_0.deleteLinCon(in sig BIGINT)
BEGIN

    DELETE FROM units_lin_con WHERE signature_from=sig;

END;
//
delimiter ;

DROP PROCEDURE IF EXISTS db_5_0.updateLinCon;
delimiter //

CREATE PROCEDURE db_5_0.updateLinCon(in sig_from BIGINT, in sig_to BIGINT, in new_a DECIMAL(65,30), in new_b_dividend BIGINT, in new_b_divisor BIGINT, in new_c DECIMAL(65,30))
BEGIN
    UPDATE units_lin_con SET signature_to=sig_to, a=new_a, b_dividend=new_b_dividend, b_divisor=new_b_divisor, c=new_c where signature_from=sig_from;

END;
//
delimiter ;


DROP PROCEDURE IF EXISTS db_5_0.cleanUpLinCon;
delimiter //

CREATE PROCEDURE db_5_0.cleanUpLinCon()
BEGIN

    DELETE FROM units_lin_con WHERE NOT EXISTS (SELECT '1' FROM double_data WHERE unit_sig=signature_from) AND NOT EXISTS (SELECT '1' FROM integer_data WHERE unit_sig=signature_from);

END;
//
delimiter ;
