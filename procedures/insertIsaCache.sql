/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
DROP PROCEDURE IF EXISTS db_5_0.insertIsa;
DELIMITER //

/**
 * Insert an "is a" relation
 *
 * This procedure fills the isa_cache table.  All passed entities must be existing
 * in entities.id.
 *
 * Parameters
 * ==========
 *
 * ChildID : VARCHAR(255)
 *     The child entity.
 *
 * ParentID : VARCHAR(255)
 *     The parent entity.
 */
CREATE PROCEDURE db_5_0.insertIsa(IN ChildID VARCHAR(255), IN ParentID VARCHAR(255))
insert_is_a_proc: BEGIN

    DECLARE c INT UNSIGNED DEFAULT NULL;
    DECLARE p INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO c FROM entity_ids WHERE id = ChildID;
    SELECT internal_id INTO p FROM entity_ids WHERE id = ParentID;

    INSERT INTO isa_cache (child, parent, rpath) VALUES (c, p, c);

    IF p = c THEN
        -- Any additional entries would be redundant.
        LEAVE insert_is_a_proc;
    END IF;
    -- Note: also for the next insertions, ignore existing lines with p == c

    -- Insert ancestors older than parents:
    -- for each supertype of p
    --     INSERT (c, supertype, p);
    INSERT IGNORE INTO isa_cache SELECT
        c
            AS child,   -- Current child
        i.parent
            AS parent,  -- Existing supertype
        IF(p=i.rpath or i.rpath=parent,  -- If ancestor=parent or parent = grandparent:
           p,                            -- New parent (directly)
           concat(p, ">", i.rpath))      -- Else "p>super.rpath"
            AS rpath
        FROM isa_cache AS i WHERE i.child = p AND i.child != i.parent;  -- Select rows with supertype

    -- Propagate to descendants:
    -- for each subtype of c: insert each supertype of p
    INSERT IGNORE INTO isa_cache SELECT
        l.child,    -- Descendant as found in isa_cache
        r.parent,   -- Ancestor as found in isa_cache
        IF(l.rpath=l.child AND r.rpath=c,  -- if distance=1 for left and right:
           c,                              -- rpath = current child
           concat(IF(l.rpath=l.child,        -- if dist=1 for descendant:
                     c,                         -- rpath starts with c
                     concat(l.rpath, '>', c)),  -- rpath starts with "desc.rpath > c"
                  IF(r.rpath=c,              -- if dist=1 for ancestor
                     '',                        -- rpath is finished
                     concat('>', r.rpath))))    -- rpath continuees with " > ancest.rpath"
            AS rpath
        FROM
            isa_cache AS l INNER JOIN isa_cache AS r
            ON (l.parent = c AND c = r.child AND l.child != l.parent); -- Left: descendants of c, right: ancestors

END;
//

DELIMITER ;
