/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

delimiter //

DROP FUNCTION IF EXISTS is_feature_config //
CREATE FUNCTION is_feature_config(
    _Key VARCHAR(255),
    Expected VARCHAR(255))
RETURNS BOOLEAN
READS SQL DATA
BEGIN
    RETURN (
        SELECT f._value = Expected FROM feature_config as f WHERE f._key = _Key
    );
END //

delimiter ;
