/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


DROP PROCEDURE IF EXISTS db_5_0.insertEntity;
delimiter //
/* Insert an Entity

Parameters
==========

EntityID : VARCHAR(255)
    The entity id.
EntityName : VARCHAR(255)

EntityDesc : TEXT

EntityRole : VARCHAR(255)
Currently one of 'RECORDTYPE', 'RECORD', 'FILE', 'PROPERTY',
'DATATYPE', 'ROLE', 'QUERYTEMPLATE'

ACL : VARBINARY(65525)

Select
======

(Version)
*/
CREATE PROCEDURE db_5_0.insertEntity(in EntityID VARCHAR(255), in EntityName VARCHAR(255), in EntityDesc TEXT, in EntityRole VARCHAR(255), in ACL VARBINARY(65525))
BEGIN
    DECLARE NewACLID INT UNSIGNED DEFAULT NULL;
    DECLARE Hash VARBINARY(255) DEFAULT NULL;
    DECLARE Version VARBINARY(255) DEFAULT NULL;
    DECLARE Transaction VARBINARY(255) DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    -- insert the acl. The new acl id is written (c-style) into the
    -- variable NewACLID.
    call entityACL(NewACLID, ACL);

    -- insert the description, role, and acl into entities table...
    INSERT INTO entities (description, role, acl)
        VALUES (EntityDesc, EntityRole, NewACLID);

    -- ... and return the generated id
    SET InternalEntityID = LAST_INSERT_ID();

    INSERT INTO entity_ids (internal_id, id) VALUES (InternalEntityID, EntityID);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        -- TODO this is transaction-scoped variable. Is this a good idea?
        SET Transaction = @SRID;
        SET Version = SHA1(UUID());
        CALL insert_single_child_version(InternalEntityID, Hash, Version, Null, Transaction);
    END IF;

    -- insert the name of the entity into name_data table
    -- 20 is the (hard-coded) id of the 'name' property.
    IF EntityName IS NOT NULL THEN
        INSERT INTO name_data
            (domain_id, entity_id, property_id, value, status, pidx)
            VALUES (0, InternalEntityID, 20, EntityName, "FIX", 0);
    END IF;

    SELECT Version as Version;

END;
//
delimiter ;
