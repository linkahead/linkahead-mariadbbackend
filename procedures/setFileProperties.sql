/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS setFileProperties;
DELIMITER //
/**
 * Update file properties.
 *
 * If ENTITY_VERSIONING is enabled the old file properties are moved to
 * `archive_files`.
 *
 * Parameters
 * ----------
 * EntityID : VARCHAR(255)
 *   The file's id.
 * FilePath : TEXT
 *   Path of the file in the internal file system.  If NULL, an existing file
 *   entity is simply deleted.
 * FileSize : BIGINT UNSIGNED
 *   Size of the file in bytes.
 * FileHash : VARCHAR(255)
 *   A Sha512 Hash of the file.
 */
CREATE PROCEDURE setFileProperties (
    in EntityID VARCHAR(255),
    in FilePath TEXT,
    in FileSize BIGINT UNSIGNED,
    in FileHash VARCHAR(255)
)
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE IVersion INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(e._iversion) INTO IVersion
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID;

        INSERT INTO archive_files (file_id, path, size, hash,
                _iversion)
            SELECT file_id, path, size, hash, IVersion AS _iversion
            FROM files
            WHERE file_id = InternalEntityID;
    END IF;

    DELETE FROM files WHERE file_id = InternalEntityID;

    IF FilePath IS NOT NULL THEN
        INSERT INTO files (file_id, path, size, hash)
            VALUES (InternalEntityID, FilePath, FileSize, unhex(FileHash));
    END IF;

END //
DELIMITER ;
