/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.getIdByName;
DELIMITER //

/*
 * Return the Entity id(s) for a given name. Optionally, filter by role and set
 * a limit.
 *
 * Parameters
 * ----------
 * Name : VARCHAR(255)
 *    The entity's name.
 * Role : VARCHAR(255)
 *    E.g. RecordType, Record, Property,...
 * Lmt : INT UNSIGNED
 *    Limit the number of returned entity ids.
 *
 * Returns
 * -------
 * EntityID : VARCHAR(255)
 */
CREATE PROCEDURE db_5_0.getIdByName(in Name VARCHAR(255), in Role VARCHAR(255), in Lmt INT UNSIGNED)
BEGIN

    SET @stmtStr = "SELECT e.id AS id FROM name_data AS n JOIN entity_ids AS e ON (n.domain_id=0 AND n.property_id=20 AND e.internal_id = n.entity_id) JOIN entities AS i ON (i.id = e.internal_id) WHERE n.value = ?";

    IF Role IS NULL THEN
        SET @stmtStr = CONCAT(@stmtStr, " AND i.role!='ROLE'");
    ELSE
        SET @stmtStr = CONCAT(@stmtStr, " AND i.role='", Role, "'");
    END IF;

    IF Lmt IS NOT NULL THEN
        SET @stmtStr = CONCAT(@stmtStr, " LIMIT ", Lmt);
    END IF;

    SET @vName = Name;
    PREPARE stmt FROM @stmtStr;
    EXECUTE stmt USING @vName;
    DEALLOCATE PREPARE stmt;

END;
//
DELIMITER ;
