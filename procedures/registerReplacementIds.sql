/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <www.indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
DROP PROCEDURE IF EXISTS db_5_0.registerSubdomain;
DROP PROCEDURE IF EXISTS db_5_0.registerReplacementIds;
delimiter //

CREATE PROCEDURE db_5_0.registerReplacementIds(in amount INT UNSIGNED)
BEGIN
    DECLARE ED INTEGER DEFAULT NULL;

    SELECT COUNT(id) INTO ED FROM entities WHERE Role='_REPLACEMENT' AND id!=0;

    WHILE ED < amount DO
        INSERT INTO entities (description, role, acl) VALUES
            (NULL, '_REPLACEMENT', 0);

        SET ED = ED + 1;
    END WHILE;

    SELECT CONCAT("$", e.id) as ReplacementID FROM entities AS e WHERE e.Role='_REPLACEMENT' and e.id!=0;

END;
//
delimiter ;
