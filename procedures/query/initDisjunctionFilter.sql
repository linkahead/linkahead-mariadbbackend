/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */



DROP PROCEDURE IF EXISTS db_5_0.initEmptyTargetSet;
DELIMITER //

CREATE PROCEDURE db_5_0.initEmptyTargetSet(in targetSet VARCHAR(255), in versioned BOOLEAN)
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT targetSet;
    IF targetSet IS NOT NULL THEN
        SET @isNotEmptyVar = NULL; /*NULL means targetSet is still empty*/
        SET @isEmptyStmtStr = CONCAT("SELECT 1 INTO @isNotEmptyVar FROM `",targetSet,"` LIMIT 1");
        PREPARE stmtIsNotEmpty FROM @isEmptyStmtStr;
        EXECUTE stmtIsNotEmpty;
        DEALLOCATE PREPARE stmtIsNotEmpty;
        IF @isNotEmptyVar IS NOT NULL THEN /*if targetSet is not empty*/
            call createTmpTable(newTableName, versioned);
        END IF;
    ELSE
        call createTmpTable(newTableName, versioned);
    END IF;
    SELECT newTableName AS newTableName;
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS db_5_0.initDisjunctionFilter;
DELIMITER //

CREATE PROCEDURE db_5_0.initDisjunctionFilter(in versioned BOOLEAN)
BEGIN
    call initEmptyTargetSet(NULL, versioned);
END;
//
DELIMITER ;
