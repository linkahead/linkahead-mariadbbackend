/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */


DROP PROCEDURE IF EXISTS db_5_0.calcComplementUnion;
-- Apparently this code is not used anymore
DELIMITER //

CREATE PROCEDURE db_5_0.calcComplementUnion(in targetSet VARCHAR(255), in subResultSet VARCHAR(255), in universe VARCHAR(255), in versioned BOOLEAN)
BEGIN
    IF versioned AND universe = "entities" THEN
        SET @stmtComplementUnionStr = CONCAT(
            'INSERT IGNORE INTO `', targetSet,
            '` SELECT e.id, _get_head_iversion(e.id) FROM entities as e WHERE NOT EXISTS ( SELECT 1 FROM `',
            subResultSet,
            '` AS diff WHERE diff.id=e.id AND diff._iversion = _get_head_iversion(e.id)) UNION ALL SELECT e.id, e._iversion FROM archive_entities AS e WHERE NOT EXISTS ( SELECT 1 FROM `',
            subResultSet,
            '` as diff WHERE e.id = diff.id AND e._iversion = diff._iversion)');
    ELSEIF versioned THEN
        SET @stmtComplementUnionStr = CONCAT(
            'INSERT IGNORE INTO `', targetSet,
            '` SELECT id FROM `',universe,
            '` AS universe WHERE NOT EXISTS ( SELECT 1 FROM `',
                subResultSet,'`
                AS diff WHERE diff.id=universe.id AND diff._iversion = universe.id_version)');
    ELSE
        SET @stmtComplementUnionStr = CONCAT('INSERT IGNORE INTO `', targetSet, '` SELECT id FROM `',universe, '` AS universe WHERE NOT EXISTS ( SELECT 1 FROM `', subResultSet,'` AS diff WHERE diff.id=universe.id)');
    END IF;
    PREPARE stmtComplementUnion FROM @stmtComplementUnionStr;
    EXECUTE stmtComplementUnion;
    DEALLOCATE PREPARE stmtComplementUnion;

END;
//

DELIMITER ;
