/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.applyIDFilter;

DELIMITER //

/**
 * Filter the sourceSet into targetSet by ID.
 *
 * This can be done by operator-value tests or by aggregate functions.
 *
 * The `versioned` flag currently only has the effect that an `_iversion` column is also copied to
 * the target.
 *
 * Parameters
 * ----------
 * sourceSet : VARCHAR(255)
 *     The name of the table from where we start.
 * targetSet : VARCHAR(255)
 *     The name of the table where we collect all the "good" ids (or NULL or same as sourceSet).
 * o : CHAR(2)
 *     The operator used for filtering, e.g. '=', '!=', '>', ...
 * EntityID : VARCHAR(255)
 *     An entity id, existing or non-existing, which we use to compare the
 *     existing entities to using the operator.
 * agg : CHAR(3)
 *     An aggregate function, e.g. 'max' or 'min'. This only makes sense for number-based ids.
 * versioned : BOOLEAN
 *     The filter belongs to a version-aware query (e.g. FIND ANY VERSION OF
 *     ...) and hence the sourceSet and targetSet have an `_iversion` column.
 */
CREATE PROCEDURE db_5_0.applyIDFilter(in sourceSet VARCHAR(255), in targetSet VARCHAR(255),
    in o CHAR(2), in EntityID VARCHAR(255), in agg CHAR(3), in versioned BOOLEAN)
IDFILTER_LABEL: BEGIN
DECLARE data VARCHAR(20000) DEFAULT NULL;
DECLARE aggVal VARCHAR(255) DEFAULT NULL;
DECLARE direction CHAR(4) DEFAULT NULL;
DECLARE entity_id_type VARCHAR(255) DEFAULT "eids.id ";

#-- get aggVal if possible
IF agg IS NOT NULL THEN
    IF versioned THEN
        -- TODO versioned queries
        SELECT 1 FROM id_agg_with_versioning_not_implemented;
    ELSEIF agg = "max" THEN
        SET direction = "DESC";
    ELSEIF agg = "min" THEN
        SET direction = "ASC ";
    ELSE
        SELECT 1 FROM unknown_agg_parameter;
    END IF;

    SET @stmtIDAggValStr = CONCAT(
        "SELECT e.internal_id INTO @sAggVal FROM `",
        sourceSet,
        "` AS s LEFT JOIN entity_ids AS e ON (s.id=e.internal_id) WHERE s.id>99 ORDER BY CAST(e.id AS UNSIGNED INT) ",
        direction,
        " LIMIT 1");

    PREPARE stmtIDAggVal FROM @stmtIDAggValStr;
    EXECUTE stmtIDAggVal;
    DEALLOCATE PREPARE stmtIDAggVal;
    SET aggVal = @sAggVal;
END IF;

IF o = ">" OR o = ">=" OR o = "<" or o = "<=" THEN
    SET entity_id_type = "CAST(eids.id AS UNSIGNED INT) ";
END IF;

#-- generate stmt string
IF targetSet IS NULL OR targetSet = sourceSet THEN
    SET data = CONCAT(
        "DELETE FROM `",
        sourceSet,
        "` WHERE ",
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT("NOT EXISTS (SELECT 1 FROM entity_ids AS eids WHERE ",
                entity_id_type,
                o,
                ' "',
                EntityID,
                '" ',
                " AND eids.internal_id = `",
                sourceSet,
                "`.id)"
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND `", sourceSet, "`.id!=",
                aggVal)));
ELSEIF versioned AND sourceSet = "entities" THEN
    -- ################# VERSIONING #####################
    SET data = CONCAT(
        "INSERT IGNORE INTO `",
        targetSet,
        '` (id, _iversion) SELECT e.id, _get_head_iversion(e.id) FROM `entities` AS e JOIN entity_ids AS eids ON (e.id = eids.internal_id) WHERE ',
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT(entity_id_type,
                o,
                ' "',
                EntityID,
                '"'
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND e.id=",
                aggVal)),
        ' UNION SELECT e.id, _iversion FROM `archive_entities` AS e JOIN entity_ids AS eids ON (e.id = eids.internal_id) WHERE ',
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT(entity_id_type,
                o,
                ' "',
                EntityID,
                '"'
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND e.id=",
                aggVal)));
    -- ##################################################

ELSE
    SET data = CONCAT(
        "INSERT IGNORE INTO `",
        targetSet,
        IF(versioned,
            '` (id, _iversion) SELECT data.id, data._iversion FROM `',
            '` (id) SELECT data.id FROM `'),
        sourceSet,
        "` AS data JOIN entity_ids AS eids ON (eids.internal_id = data.id) WHERE ",
        IF(o IS NULL OR EntityID IS NULL,
            "1=1",
            CONCAT(entity_id_type,
                o,
                ' "',
                EntityID,
                '"'
            )),
        IF(aggVal IS NULL,
            "",
            CONCAT(" AND data.id=",
                aggVal)));
END IF;

Set @stmtIDFilterStr = data;
PREPARE stmtIDFilter FROM @stmtIDFilterStr;
EXECUTE stmtIDFilter;
DEALLOCATE PREPARE stmtIDFilter;

END;
//

DELIMITER ;
