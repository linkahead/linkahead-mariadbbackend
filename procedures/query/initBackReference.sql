/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.initBackReference;
DELIMITER //

/*
 * Create and initialize two new temporary tables.
 *
 * This is used to initialize the filterin for backreferences to a
 * particular entity (specified by EntityID or EntityName) or using a
 * particular property (specified by PropertyID or PropertyName).
 *
 * The propertiesTable contains all properties matching the PropertyID or
 * PropertyName. The entitiesTable contains all entities matching EntityID or
 * EntityName.

 * Parameters
 * ----------
 * PropertyID : VARCHAR(255)
 *     The property's (external) id.
 * PropertyName : VARCHAR(255)
 *     The property's name.
 * EntityID : VARCHAR(255)
 *     The entity's (external) id.
 * EntityName : VARCHAR(255)
 *     The entity's name.
 *
 * Select
 * ------
 * Tuple (propertiesTable, entitiesTable)
 */
CREATE PROCEDURE db_5_0.initBackReference(in PropertyID VARCHAR(255), in PropertyName VARCHAR(255), in EntityID VARCHAR(255), in EntityName VARCHAR(255))
BEGIN
    DECLARE propertiesTable VARCHAR(255) DEFAULT NULL;
    DECLARE entitiesTable VARCHAR(255) DEFAULT NULL;

    IF PropertyName IS NOT NULL THEN
        -- TODO versioning for properties
        call createTmpTable(propertiesTable, FALSE);
        call initSubEntity(PropertyID, PropertyName, propertiesTable);
    END IF;

    IF EntityName IS NOT NULL THEN
        -- TODO versioning for referencing entities
        call createTmpTable(entitiesTable, FALSE);
        call initSubEntity(EntityID, EntityName, entitiesTable);
    END IF;

    SELECT propertiesTable, entitiesTable;


END //
DELIMITER ;
