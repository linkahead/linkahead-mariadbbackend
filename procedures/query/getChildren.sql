/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

Drop Procedure if exists db_5_0.getChildren;
Delimiter //
/**
 * Find and return all children of the IDs given in `tableName`.
 *
 * If the `versioned` parameter is `TRUE`, also all archived (non-HEAD) children are added.
 */
Create Procedure db_5_0.getChildren(in tableName varchar(255), in versioned BOOLEAN)

BEGIN
    DECLARE found_children INT UNSIGNED DEFAULT 0;

    DROP TEMPORARY TABLE IF EXISTS dependTemp;
    CREATE TEMPORARY TABLE dependTemp (id INT UNSIGNED, _iversion INT UNSIGNED, PRIMARY KEY(id, _iversion));


    SET @initDepend = CONCAT(
        'INSERT IGNORE INTO dependTemp (id, _iversion) SELECT i.child, ',
        IF(versioned,
            '_get_head_iversion(i.child)',
            '0'),
        ' FROM isa_cache AS i INNER JOIN `',
        tableName,
        '` AS t ON (i.parent=t.id);');
    PREPARE initDependStmt FROM @initDepend;

    EXECUTE initDependStmt;
    SET found_children = found_children + ROW_COUNT();

    -- ################# VERSIONING #####################

    IF versioned IS TRUE THEN
        SET @initDepend = CONCAT(
            'INSERT IGNORE INTO dependTemp (id, _iversion) ',
            'SELECT i.child, i.child_iversion FROM archive_isa AS i INNER JOIN `',
            tableName,
            '` AS t ON (i.parent=t.id);');
        PREPARE initDependStmt FROM @initDepend;

        EXECUTE initDependStmt;
        SET found_children = found_children + ROW_COUNT();
    END IF;

    -- ##################################################


    IF found_children != 0 THEN
        SET @transfer = CONCAT(
            'INSERT IGNORE INTO `',
            tableName,
            IF(versioned,
                '` (id, _iversion) SELECT id, _iversion FROM dependTemp',
                '` (id) SELECT id FROM dependTemp'));
        PREPARE transferstmt FROM @transfer;
        EXECUTE transferstmt;
        DEALLOCATE PREPARE transferstmt;
    END IF;


    DEALLOCATE PREPARE initDependStmt;

END;
//
delimiter ;
