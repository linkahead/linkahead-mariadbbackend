/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_5_0.applyBackReference;
DELIMITER //

/**
 * Process a set of entities, keeping those which are referenced in a specific way.
 *
 * This procedure filters a source table and writes the result into a target table.  If
 * the target table is not given, all non-matching entries are deleted from the source table
 * instead.
 *
 * Candidate entities are allowed to pass if there is an entity from the entities table which
 * references the candidate as one of the properties in the properties table.
 *
 * Parameters
 * ----------
 * sourceSet : table
 * The name of the table which shall be filtered, must have column `id`.
 *
 * targetSet : table
 * The name of the result table, must have column `id`.
 *
 * propertiesTable : table
 * References as Properties in this table are counted.
 *
 * entitiesTable : table
 * References by Entities in this table are counted.
 *
 * subQuery : boolean
 * Create a temporary target table and select as `list`, instead of using the given targetSet.  The
 * `versioned` parameter has no effect in this case.
 *
 * versioned : boolean
 * If True, if a reference is versioned (references to specific versions of entities), the target
 * candidate's version must match.  Therefore, the sourceSet and targetSet must have a `_iversion`
 * column as well (unless sourceSet is the `entities` table).
 *
 */
CREATE PROCEDURE db_5_0.applyBackReference(in sourceSet VARCHAR(255), targetSet VARCHAR(255),
    in propertiesTable VARCHAR(255), in entitiesTable VARCHAR(255), in subQuery BOOLEAN,
    in versioned BOOLEAN)
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;


    IF subQuery IS TRUE THEN
        call registerTempTableName(newTableName);

        SET @createBackRefSubQueryTableStr = CONCAT('CREATE TEMPORARY TABLE `',newTableName,'` ( entity_id INT UNSIGNED NOT NULL, id INT UNSIGNED NOT NULL, CONSTRAINT `',newTableName,'PK` PRIMARY KEY (id, entity_id))');

        PREPARE createBackRefSubQueryTable FROM @createBackRefSubQueryTableStr;
        EXECUTE createBackRefSubQueryTable;
        DEALLOCATE PREPARE createBackRefSubQueryTable;

        SET @backRefSubResultSetStmtStr = CONCAT('INSERT IGNORE INTO `',
            newTableName,
            '` (id,entity_id) SELECT entity_id AS id, value AS entity_id FROM `reference_data` AS data ',
            'WHERE EXISTS (SELECT 1 FROM `',
                sourceSet,
                '` AS source WHERE source.id=data.value LIMIT 1)',
            IF(propertiesTable IS NULL,
                '',
                CONCAT(' AND EXISTS (SELECT 1 FROM `',
                    propertiesTable,
                    '` AS p WHERE p.id=data.property_id LIMIT 1)')),
            IF(entitiesTable IS NULL,
                '',
                CONCAT(' AND EXISTS (SELECT 1 FROM `',
                    entitiesTable,
                    '` AS e WHERE e.id=data.entity_id LIMIT 1)'))
        );

        PREPARE backRefSubResultSetStmt FROM @backRefSubResultSetStmtStr;
        EXECUTE backRefSubResultSetStmt;
        DEALLOCATE PREPARE backRefSubResultSetStmt;

        SELECT newTableName as list;
    ELSE
        IF versioned THEN
            IF sourceSet = "entities" THEN
                -- Find any referenced entity, current or archived
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, _get_head_iversion(source.id)',
                    -- current entities
                    ' FROM entities AS source WHERE EXISTS (',
                        'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND (',
                            'data.value_iversion IS NULL OR data.value_iversion=_get_head_iversion(source.id))',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ') UNION ALL ',
                    -- and archived entities
                    'SELECT source.id, source._iversion FROM archive_entities AS source WHERE EXISTS (',
                        'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND ',
                          '(data.value_iversion IS NULL OR data.value_iversion=source._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),

                    ')');
            ELSEIF targetSet IS NULL OR sourceSet = targetSet THEN
                SET @stmtBackRefStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=`',
                    sourceSet,
                    '`.`id` AND ( data.value_iversion IS NULL OR data.value_iversion=`',
                    sourceSet,
                    '`._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            ELSE
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id, _iversion) SELECT source.id, source._iversion FROM `',
                    sourceSet,
                    '` AS source WHERE EXISTS (',
                    'SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id AND',
                    ' (data.value_iversion IS NULL OR data.value_iversion=source._iversion)',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),

                    ')');
            END IF;
        ELSE
            -- unversioned queries
            IF targetSet IS NULL OR sourceSet = targetSet THEN
                -- delete from sourceSet
                SET @stmtBackRefStr = CONCAT('DELETE FROM `',
                    sourceSet,
                    '` WHERE NOT EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=`',
                    sourceSet,
                    '`.`id`',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT('
                            AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT('
                            AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            ELSE
                -- write to targetSet
                SET @stmtBackRefStr = CONCAT('INSERT IGNORE INTO `',
                    targetSet,
                    '` (id) SELECT id FROM `',
                    sourceSet,
                    '` AS source WHERE EXISTS (SELECT 1 FROM `reference_data` AS data WHERE data.value=source.id',
                    IF(entitiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            entitiesTable,
                            '` AS e WHERE e.id=data.entity_id LIMIT 1)')),
                    IF(propertiesTable IS NULL,
                        '',
                        CONCAT(' AND EXISTS (SELECT 1 FROM `',
                            propertiesTable,
                            '` AS p WHERE p.id=data.property_id LIMIT 1)')),
                    ')');
            END IF;
        END IF;

        PREPARE stmtBackRef FROM @stmtBackRefStr;
        EXECUTE stmtBackRef;
        DEALLOCATE PREPARE stmtBackRef;
    END IF;

END;
//

DELIMITER ;
