-- ***************************************************************
-- myTAP Testing Script
-- ====================
-- Generated: 2020-02-04
-- This database testing script has been created by the autotap
-- utility. The tests generated are in the TAP format for use with
-- myTAP and are based on the current state of schema objects,
-- with the database assumed to be in a known good state. All tests
-- will pass but that does not guarantee the correctness of the
-- state represented by the tests.
-- 
-- After the script is generated, any subsequent DDL changes,
-- whether additions, deletions or modifications, will cause some
-- of the tests to fail. This is intentional.
-- 
-- The purpose of the utility is to assist the process of
-- retrofitting testing to existing projects. You should still check
-- that the schema state represents what you intend and you should
-- modify this script by hand to account for all subsequent changes.
-- ***************************************************************


-- ***************************************************************
-- SCHEMA _caosdb_schema_unit_tests
-- ***************************************************************

USE tap;
BEGIN;
CALL tap.no_plan();
SELECT tap.has_schema('_caosdb_schema_unit_tests','');

-- ***************************************************************
-- TABLES 
-- ***************************************************************

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.archive_isa
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','archive_isa','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','archive_isa','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','archive_isa','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','archive_isa','`child`,`child_iversion`,`parent`,`direct`','');

-- COLUMN archive_isa.child

SELECT tap.has_column('_caosdb_schema_unit_tests','archive_isa','child','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','archive_isa','child','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','archive_isa','child','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','archive_isa','child',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','archive_isa','child',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','archive_isa','child',NULL,'');

-- COLUMN archive_isa.child_iversion

SELECT tap.has_column('_caosdb_schema_unit_tests','archive_isa','child_iversion','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','archive_isa','child_iversion','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','archive_isa','child_iversion','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','archive_isa','child_iversion',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','archive_isa','child_iversion',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','archive_isa','child_iversion',NULL,'');

-- COLUMN archive_isa.parent

SELECT tap.has_column('_caosdb_schema_unit_tests','archive_isa','parent','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','archive_isa','parent','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','archive_isa','parent','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','archive_isa','parent',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','archive_isa','parent',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','archive_isa','parent',NULL,'');

-- COLUMN archive_isa.direct

SELECT tap.has_column('_caosdb_schema_unit_tests','archive_isa','direct','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','archive_isa','direct','tinyint(1)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','archive_isa','direct','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','archive_isa','direct',1,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','archive_isa','direct',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','archive_isa','direct',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','archive_isa','`parent`,`child`','');

-- INDEX archive_isa.child

SELECT tap.has_index('_caosdb_schema_unit_tests','archive_isa','child','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','archive_isa','child','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','archive_isa','`child`,`child_iversion`','');

-- INDEX archive_isa.parent

SELECT tap.has_index('_caosdb_schema_unit_tests','archive_isa','parent','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','archive_isa','parent','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','archive_isa','`parent`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','archive_isa','`archive_isa_ibfk_1`,`archive_isa_ibfk_2`','');

-- CONSTRAINT archive_isa.archive_isa_ibfk_1

SELECT tap.has_constraint('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_1','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_1','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_1','CASCADE','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_1','RESTRICT','');

-- CONSTRAINT archive_isa.archive_isa_ibfk_2

SELECT tap.has_constraint('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_2','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_2','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_2','CASCADE','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','archive_isa','archive_isa_ibfk_2','RESTRICT','');
-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.collection_type
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','collection_type','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','collection_type','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','collection_type','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','collection_type','`domain_id`,`entity_id`,`property_id`,`collection`','');

-- COLUMN collection_type.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','collection_type','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','collection_type','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','collection_type','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','collection_type','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','collection_type','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','collection_type','domain_id',NULL,'');

-- COLUMN collection_type.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','collection_type','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','collection_type','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','collection_type','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','collection_type','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','collection_type','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','collection_type','entity_id',NULL,'');

-- COLUMN collection_type.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','collection_type','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','collection_type','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','collection_type','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','collection_type','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','collection_type','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','collection_type','property_id',NULL,'');

-- COLUMN collection_type.collection

SELECT tap.has_column('_caosdb_schema_unit_tests','collection_type','collection','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','collection_type','collection','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','collection_type','collection','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','collection_type','collection',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','collection_type','collection','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','collection_type','collection','utf8mb3_unicode_ci','');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','collection_type','`domain_id`,`entity_id`,`property_id`','');

-- INDEX collection_type.domain_id

SELECT tap.has_index('_caosdb_schema_unit_tests','collection_type','domain_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','collection_type','domain_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','collection_type','`domain_id`,`entity_id`','');

-- INDEX collection_type.entity_id

SELECT tap.has_index('_caosdb_schema_unit_tests','collection_type','entity_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','collection_type','entity_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','collection_type','`entity_id`','');

-- INDEX collection_type.property_id

SELECT tap.has_index('_caosdb_schema_unit_tests','collection_type','property_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','collection_type','property_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','collection_type','`property_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','collection_type','`collection_type-d-e-p`,`collection_type_domain_id_entity`,`collection_type_entity_id_entity`,`collection_type_property_id_entity`','');

-- CONSTRAINT collection_type.collection_type_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','collection_type','collection_type_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','collection_type','collection_type_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','collection_type','collection_type_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','collection_type','collection_type_domain_id_entity','RESTRICT','');

-- CONSTRAINT collection_type.collection_type_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','collection_type','collection_type_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','collection_type','collection_type_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','collection_type','collection_type_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','collection_type','collection_type_entity_id_entity','RESTRICT','');

-- CONSTRAINT collection_type.collection_type_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','collection_type','collection_type_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','collection_type','collection_type_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','collection_type','collection_type_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','collection_type','collection_type_property_id_entity','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.data_type
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','data_type','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','data_type','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','data_type','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','data_type','`domain_id`,`entity_id`,`property_id`,`datatype`','');

-- COLUMN data_type.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','data_type','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','data_type','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','data_type','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','data_type','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','data_type','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','data_type','domain_id',NULL,'');

-- COLUMN data_type.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','data_type','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','data_type','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','data_type','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','data_type','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','data_type','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','data_type','entity_id',NULL,'');

-- COLUMN data_type.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','data_type','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','data_type','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','data_type','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','data_type','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','data_type','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','data_type','property_id',NULL,'');

-- COLUMN data_type.datatype

SELECT tap.has_column('_caosdb_schema_unit_tests','data_type','datatype','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','data_type','datatype','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','data_type','datatype','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','data_type','datatype',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','data_type','datatype',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','data_type','datatype',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','data_type','`name_ov_dom_ent_idx`','');

-- INDEX data_type.name_ov_dom_ent_idx

SELECT tap.has_index('_caosdb_schema_unit_tests','data_type','name_ov_dom_ent_idx','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','data_type','name_ov_dom_ent_idx','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','data_type','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','data_type','`datatype_ukey`,`datatype_forkey_dom`,`datatype_forkey_ent`,`datatype_forkey_pro`,`datatype_forkey_type`','');

-- CONSTRAINT data_type.datatype_forkey_dom

SELECT tap.has_constraint('_caosdb_schema_unit_tests','data_type','datatype_forkey_dom','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','data_type','datatype_forkey_dom','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','data_type','datatype_forkey_dom','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','data_type','datatype_forkey_dom','RESTRICT','');

-- CONSTRAINT data_type.datatype_forkey_ent

SELECT tap.has_constraint('_caosdb_schema_unit_tests','data_type','datatype_forkey_ent','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','data_type','datatype_forkey_ent','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','data_type','datatype_forkey_ent','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','data_type','datatype_forkey_ent','RESTRICT','');

-- CONSTRAINT data_type.datatype_forkey_pro

SELECT tap.has_constraint('_caosdb_schema_unit_tests','data_type','datatype_forkey_pro','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','data_type','datatype_forkey_pro','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','data_type','datatype_forkey_pro','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','data_type','datatype_forkey_pro','RESTRICT','');

-- CONSTRAINT data_type.datatype_forkey_type

SELECT tap.has_constraint('_caosdb_schema_unit_tests','data_type','datatype_forkey_type','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','data_type','datatype_forkey_type','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','data_type','datatype_forkey_type','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','data_type','datatype_forkey_type','RESTRICT','');

-- CONSTRAINT data_type.datatype_ukey

SELECT tap.has_constraint('_caosdb_schema_unit_tests','data_type','datatype_ukey','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','data_type','datatype_ukey','UNIQUE','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.date_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','date_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','date_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','date_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','date_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`','');

-- COLUMN date_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','date_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','date_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','date_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','date_data','domain_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','date_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','date_data','domain_id',NULL,'');

-- COLUMN date_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','date_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','date_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','date_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','date_data','entity_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','date_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','date_data','entity_id',NULL,'');

-- COLUMN date_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','date_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','date_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','date_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','date_data','property_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','date_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','date_data','property_id',NULL,'');

-- COLUMN date_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','date_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','date_data','value','int(11)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','date_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','date_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','date_data','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','date_data','value',NULL,'');

-- COLUMN date_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','date_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','date_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','date_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','date_data','status','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','date_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','date_data','status','utf8mb3_unicode_ci','');

-- COLUMN date_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','date_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','date_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','date_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','date_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','date_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','date_data','pidx',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','date_data','`date_data_dom_ent_idx`','');

-- INDEX date_data.date_data_dom_ent_idx

SELECT tap.has_index('_caosdb_schema_unit_tests','date_data','date_data_dom_ent_idx','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','date_data','date_data_dom_ent_idx','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','date_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','date_data','`date_ov_forkey_dom`,`date_ov_forkey_ent`,`date_ov_forkey_pro`','');

-- CONSTRAINT date_data.date_ov_forkey_dom

SELECT tap.has_constraint('_caosdb_schema_unit_tests','date_data','date_ov_forkey_dom','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','date_data','date_ov_forkey_dom','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','date_data','date_ov_forkey_dom','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','date_data','date_ov_forkey_dom','RESTRICT','');

-- CONSTRAINT date_data.date_ov_forkey_ent

SELECT tap.has_constraint('_caosdb_schema_unit_tests','date_data','date_ov_forkey_ent','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','date_data','date_ov_forkey_ent','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','date_data','date_ov_forkey_ent','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','date_data','date_ov_forkey_ent','RESTRICT','');

-- CONSTRAINT date_data.date_ov_forkey_pro

SELECT tap.has_constraint('_caosdb_schema_unit_tests','date_data','date_ov_forkey_pro','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','date_data','date_ov_forkey_pro','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','date_data','date_ov_forkey_pro','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','date_data','date_ov_forkey_pro','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.datetime_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','datetime_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','datetime_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','datetime_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','datetime_data','`domain_id`,`entity_id`,`property_id`,`status`,`pidx`,`value_ns`,`value`','');

-- COLUMN datetime_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','domain_id',NULL,'');

-- COLUMN datetime_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','entity_id',NULL,'');

-- COLUMN datetime_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','property_id',NULL,'');

-- COLUMN datetime_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\',\'REPLACEMENT\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','status',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','status','utf8mb3_unicode_ci','');

-- COLUMN datetime_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','pidx',NULL,'');

-- COLUMN datetime_data.value_ns

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','value_ns','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','value_ns','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','value_ns','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','value_ns','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','value_ns',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','value_ns',NULL,'');

-- COLUMN datetime_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','datetime_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','datetime_data','value','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','datetime_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','datetime_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','datetime_data','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','datetime_data','value',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','datetime_data','`domain_id`','');

-- INDEX datetime_data.domain_id

SELECT tap.has_index('_caosdb_schema_unit_tests','datetime_data','domain_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','datetime_data','domain_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','datetime_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','datetime_data','`dat_domain_id_entity`,`dat_entity_id_entity`,`dat_property_id_entity`','');

-- CONSTRAINT datetime_data.dat_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','datetime_data','dat_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','datetime_data','dat_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','datetime_data','dat_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','datetime_data','dat_domain_id_entity','RESTRICT','');

-- CONSTRAINT datetime_data.dat_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','datetime_data','dat_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','datetime_data','dat_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','datetime_data','dat_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','datetime_data','dat_entity_id_entity','RESTRICT','');

-- CONSTRAINT datetime_data.dat_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','datetime_data','dat_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','datetime_data','dat_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','datetime_data','dat_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','datetime_data','dat_property_id_entity','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.desc_overrides
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','desc_overrides','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','desc_overrides','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','desc_overrides','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','desc_overrides','`domain_id`,`entity_id`,`property_id`,`description`','');

-- COLUMN desc_overrides.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','desc_overrides','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','desc_overrides','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','desc_overrides','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','desc_overrides','domain_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','desc_overrides','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','desc_overrides','domain_id',NULL,'');

-- COLUMN desc_overrides.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','desc_overrides','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','desc_overrides','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','desc_overrides','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','desc_overrides','entity_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','desc_overrides','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','desc_overrides','entity_id',NULL,'');

-- COLUMN desc_overrides.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','desc_overrides','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','desc_overrides','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','desc_overrides','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','desc_overrides','property_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','desc_overrides','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','desc_overrides','property_id',NULL,'');

-- COLUMN desc_overrides.description

SELECT tap.has_column('_caosdb_schema_unit_tests','desc_overrides','description','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','desc_overrides','description','text','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','desc_overrides','description','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','desc_overrides','description','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','desc_overrides','description','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','desc_overrides','description','utf8mb3_unicode_ci','');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','desc_overrides','`desc_ov_dom_ent_idx`','');

-- INDEX desc_overrides.desc_ov_dom_ent_idx

SELECT tap.has_index('_caosdb_schema_unit_tests','desc_overrides','desc_ov_dom_ent_idx','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','desc_overrides','desc_ov_dom_ent_idx','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','desc_overrides','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','desc_overrides','`desc_ov_ukey`,`desc_ov_forkey_dom`,`desc_ov_forkey_ent`,`desc_ov_forkey_pro`','');

-- CONSTRAINT desc_overrides.desc_ov_forkey_dom

SELECT tap.has_constraint('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_dom','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_dom','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_dom','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_dom','RESTRICT','');

-- CONSTRAINT desc_overrides.desc_ov_forkey_ent

SELECT tap.has_constraint('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_ent','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_ent','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_ent','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_ent','RESTRICT','');

-- CONSTRAINT desc_overrides.desc_ov_forkey_pro

SELECT tap.has_constraint('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_pro','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_pro','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_pro','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','desc_overrides','desc_ov_forkey_pro','RESTRICT','');

-- CONSTRAINT desc_overrides.desc_ov_ukey

SELECT tap.has_constraint('_caosdb_schema_unit_tests','desc_overrides','desc_ov_ukey','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','desc_overrides','desc_ov_ukey','UNIQUE','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.double_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','double_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','double_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','double_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','double_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`,`unit_sig`','');

-- COLUMN double_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','domain_id',NULL,'');

-- COLUMN double_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','entity_id',NULL,'');

-- COLUMN double_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','property_id',NULL,'');

-- COLUMN double_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','value','double','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','value',NULL,'');

-- COLUMN double_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\',\'REPLACEMENT\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','status',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','status','utf8mb3_unicode_ci','');

-- COLUMN double_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','pidx',NULL,'');

-- COLUMN double_data.unit_sig

SELECT tap.has_column('_caosdb_schema_unit_tests','double_data','unit_sig','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','double_data','unit_sig','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','double_data','unit_sig','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','double_data','unit_sig','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','double_data','unit_sig',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','double_data','unit_sig',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','double_data','`domain_id`','');

-- INDEX double_data.domain_id

SELECT tap.has_index('_caosdb_schema_unit_tests','double_data','domain_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','double_data','domain_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','double_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','double_data','`dou_domain_id_entity`,`dou_entity_id_entity`,`dou_property_id_entity`','');

-- CONSTRAINT double_data.dou_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','double_data','dou_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','double_data','dou_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','double_data','dou_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','double_data','dou_domain_id_entity','RESTRICT','');

-- CONSTRAINT double_data.dou_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','double_data','dou_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','double_data','dou_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','double_data','dou_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','double_data','dou_entity_id_entity','RESTRICT','');

-- CONSTRAINT double_data.dou_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','double_data','dou_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','double_data','dou_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','double_data','dou_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','double_data','dou_property_id_entity','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.entities
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','entities','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','entities','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','entities','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','entities','`id`,`description`,`role`,`acl`','');

-- COLUMN entities.id

SELECT tap.has_column('_caosdb_schema_unit_tests','entities','id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entities','id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entities','id','auto_increment','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entities','id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entities','id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entities','id',NULL,'');

-- COLUMN entities.description

SELECT tap.has_column('_caosdb_schema_unit_tests','entities','description','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entities','description','text','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entities','description','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entities','description','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entities','description','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entities','description','utf8mb3_unicode_ci','');

-- COLUMN entities.role

SELECT tap.has_column('_caosdb_schema_unit_tests','entities','role','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entities','role','enum(\'RECORDTYPE\',\'RECORD\',\'FILE\',\'_REPLACEMENT\',\'PROPERTY\',\'DATATYPE\',\'ROLE\',\'QUERYTEMPLATE\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entities','role','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entities','role',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entities','role','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entities','role','utf8mb3_unicode_ci','');

-- COLUMN entities.acl

SELECT tap.has_column('_caosdb_schema_unit_tests','entities','acl','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entities','acl','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entities','acl','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entities','acl','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entities','acl',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entities','acl',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','entities','`PRIMARY`,`entity_entity_acl`','');

-- CONSTRAINT entities.entity_entity_acl

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entities','entity_entity_acl','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entities','entity_entity_acl','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','entities','entity_entity_acl','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','entities','entity_entity_acl','RESTRICT','');

-- CONSTRAINT entities.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entities','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entities','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','entities','`id`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.entity_acl
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','entity_acl','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','entity_acl','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','entity_acl','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','entity_acl','`id`,`acl`','');

-- COLUMN entity_acl.id

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_acl','id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_acl','id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_acl','id','auto_increment','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_acl','id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_acl','id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_acl','id',NULL,'');

-- COLUMN entity_acl.acl

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_acl','acl','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_acl','acl','varbinary(65525)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_acl','acl','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_acl','acl',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_acl','acl',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_acl','acl',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','entity_acl','`entity_acl_acl`','');

-- INDEX entity_acl.entity_acl_acl

SELECT tap.has_index('_caosdb_schema_unit_tests','entity_acl','entity_acl_acl','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','entity_acl','entity_acl_acl','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','entity_acl','`acl`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','entity_acl','`PRIMARY`','');

-- CONSTRAINT entity_acl.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entity_acl','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entity_acl','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','entity_acl','`id`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.enum_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','enum_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','enum_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','enum_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','enum_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`','');

-- COLUMN enum_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','enum_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','enum_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','enum_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','enum_data','domain_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','enum_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','enum_data','domain_id',NULL,'');

-- COLUMN enum_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','enum_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','enum_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','enum_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','enum_data','entity_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','enum_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','enum_data','entity_id',NULL,'');

-- COLUMN enum_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','enum_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','enum_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','enum_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','enum_data','property_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','enum_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','enum_data','property_id',NULL,'');

-- COLUMN enum_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','enum_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','enum_data','value','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','enum_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','enum_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','enum_data','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','enum_data','value',NULL,'');

-- COLUMN enum_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','enum_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','enum_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','enum_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','enum_data','status','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','enum_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','enum_data','status','utf8mb3_unicode_ci','');

-- COLUMN enum_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','enum_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','enum_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','enum_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','enum_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','enum_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','enum_data','pidx',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','enum_data','`enum_ov_dom_ent_idx`','');

-- INDEX enum_data.enum_ov_dom_ent_idx

SELECT tap.has_index('_caosdb_schema_unit_tests','enum_data','enum_ov_dom_ent_idx','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','enum_data','enum_ov_dom_ent_idx','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','enum_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','enum_data','`enum_ov_forkey_dom`,`enum_ov_forkey_ent`,`enum_ov_forkey_pro`','');

-- CONSTRAINT enum_data.enum_ov_forkey_dom

SELECT tap.has_constraint('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_dom','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_dom','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_dom','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_dom','RESTRICT','');

-- CONSTRAINT enum_data.enum_ov_forkey_ent

SELECT tap.has_constraint('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_ent','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_ent','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_ent','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_ent','RESTRICT','');

-- CONSTRAINT enum_data.enum_ov_forkey_pro

SELECT tap.has_constraint('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_pro','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_pro','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_pro','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','enum_data','enum_ov_forkey_pro','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.files
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','files','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','files','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','files','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','files','`file_id`,`path`,`size`,`hash`,`checked_timestamp`','');

-- COLUMN files.file_id

SELECT tap.has_column('_caosdb_schema_unit_tests','files','file_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','files','file_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','files','file_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','files','file_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','files','file_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','files','file_id',NULL,'');

-- COLUMN files.path

SELECT tap.has_column('_caosdb_schema_unit_tests','files','path','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','files','path','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','files','path','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','files','path',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','files','path','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','files','path','utf8mb3_unicode_ci','');

-- COLUMN files.size

SELECT tap.has_column('_caosdb_schema_unit_tests','files','size','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','files','size','bigint(20) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','files','size','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','files','size',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','files','size',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','files','size',NULL,'');

-- COLUMN files.hash

SELECT tap.has_column('_caosdb_schema_unit_tests','files','hash','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','files','hash','binary(64)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','files','hash','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','files','hash','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','files','hash',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','files','hash',NULL,'');

-- COLUMN files.checked_timestamp

SELECT tap.has_column('_caosdb_schema_unit_tests','files','checked_timestamp','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','files','checked_timestamp','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','files','checked_timestamp','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','files','checked_timestamp',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','files','checked_timestamp',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','files','checked_timestamp',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','files','`PRIMARY`,`fil_file_id_entity`','');

-- CONSTRAINT files.fil_file_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','files','fil_file_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','files','fil_file_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','files','fil_file_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','files','fil_file_id_entity','RESTRICT','');

-- CONSTRAINT files.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','files','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','files','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','files','`file_id`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.integer_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','integer_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','integer_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','integer_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','integer_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`,`unit_sig`','');

-- COLUMN integer_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','domain_id',NULL,'');

-- COLUMN integer_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','entity_id',NULL,'');

-- COLUMN integer_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','property_id',NULL,'');

-- COLUMN integer_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','value','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','value',NULL,'');

-- COLUMN integer_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\',\'REPLACEMENT\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','status',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','status','utf8mb3_unicode_ci','');

-- COLUMN integer_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','pidx',NULL,'');

-- COLUMN integer_data.unit_sig

SELECT tap.has_column('_caosdb_schema_unit_tests','integer_data','unit_sig','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','integer_data','unit_sig','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','integer_data','unit_sig','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','integer_data','unit_sig','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','integer_data','unit_sig',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','integer_data','unit_sig',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','integer_data','`domain_id`','');

-- INDEX integer_data.domain_id

SELECT tap.has_index('_caosdb_schema_unit_tests','integer_data','domain_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','integer_data','domain_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','integer_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','integer_data','`int_domain_id_entity`,`int_entity_id_entity`,`int_property_id_entity`','');

-- CONSTRAINT integer_data.int_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','integer_data','int_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','integer_data','int_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','integer_data','int_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','integer_data','int_domain_id_entity','RESTRICT','');

-- CONSTRAINT integer_data.int_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','integer_data','int_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','integer_data','int_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','integer_data','int_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','integer_data','int_entity_id_entity','RESTRICT','');

-- CONSTRAINT integer_data.int_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','integer_data','int_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','integer_data','int_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','integer_data','int_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','integer_data','int_property_id_entity','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.isa_cache
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','isa_cache','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','isa_cache','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','isa_cache','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','isa_cache','`child`,`parent`,`rpath`','');

-- COLUMN isa_cache.child

SELECT tap.has_column('_caosdb_schema_unit_tests','isa_cache','child','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','isa_cache','child','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','isa_cache','child','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','isa_cache','child',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','isa_cache','child',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','isa_cache','child',NULL,'');

-- COLUMN isa_cache.parent

SELECT tap.has_column('_caosdb_schema_unit_tests','isa_cache','parent','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','isa_cache','parent','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','isa_cache','parent','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','isa_cache','parent',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','isa_cache','parent',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','isa_cache','parent',NULL,'');

-- COLUMN isa_cache.rpath

SELECT tap.has_column('_caosdb_schema_unit_tests','isa_cache','rpath','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','isa_cache','rpath','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','isa_cache','rpath','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','isa_cache','rpath',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','isa_cache','rpath','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','isa_cache','rpath','utf8mb3_unicode_ci','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','isa_cache','`PRIMARY`,`isa_cache_child_entity`,`isa_cache_parent_entity`','');

-- CONSTRAINT isa_cache.isa_cache_child_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','isa_cache','isa_cache_child_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','isa_cache','isa_cache_child_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','isa_cache','isa_cache_child_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','isa_cache','isa_cache_child_entity','RESTRICT','');

-- CONSTRAINT isa_cache.isa_cache_parent_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','isa_cache','isa_cache_parent_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','isa_cache','isa_cache_parent_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','isa_cache','isa_cache_parent_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','isa_cache','isa_cache_parent_entity','RESTRICT','');

-- CONSTRAINT isa_cache.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','isa_cache','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','isa_cache','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','isa_cache','`child`,`parent`,`rpath`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.name_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','name_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','name_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','name_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','name_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`','');

-- COLUMN name_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','name_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_data','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_data','domain_id',NULL,'');

-- COLUMN name_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','name_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_data','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_data','entity_id',NULL,'');

-- COLUMN name_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','name_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_data','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_data','property_id',NULL,'');

-- COLUMN name_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','name_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_data','value','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_data','value','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_data','value','utf8mb3_unicode_ci','');

-- COLUMN name_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','name_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\',\'REPLACEMENT\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_data','status',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_data','status','utf8mb3_unicode_ci','');

-- COLUMN name_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','name_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_data','pidx',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','name_data','`domain_id`,`entity_id`,`property_id`,`value`','');

-- INDEX name_data.domain_id

SELECT tap.has_index('_caosdb_schema_unit_tests','name_data','domain_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','name_data','domain_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','name_data','`domain_id`,`entity_id`','');

-- INDEX name_data.entity_id

SELECT tap.has_index('_caosdb_schema_unit_tests','name_data','entity_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','name_data','entity_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','name_data','`entity_id`','');

-- INDEX name_data.property_id

SELECT tap.has_index('_caosdb_schema_unit_tests','name_data','property_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','name_data','property_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','name_data','`property_id`','');

-- INDEX name_data.value

SELECT tap.has_index('_caosdb_schema_unit_tests','name_data','value','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','name_data','value','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','name_data','`value`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','name_data','`name_data_domain_id_entity`,`domain_id_2`,`name_data_entity_id_entity`,`name_data_property_id_entity`','');

-- CONSTRAINT name_data.name_data_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_data','name_data_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_data','name_data_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','name_data','name_data_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','name_data','name_data_domain_id_entity','RESTRICT','');

-- CONSTRAINT name_data.domain_id_2

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_data','domain_id_2','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_data','domain_id_2','UNIQUE','');

-- CONSTRAINT name_data.name_data_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_data','name_data_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_data','name_data_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','name_data','name_data_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','name_data','name_data_entity_id_entity','RESTRICT','');

-- CONSTRAINT name_data.name_data_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_data','name_data_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_data','name_data_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','name_data','name_data_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','name_data','name_data_property_id_entity','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.name_overrides
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','name_overrides','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','name_overrides','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','name_overrides','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','name_overrides','`domain_id`,`entity_id`,`property_id`,`name`','');

-- COLUMN name_overrides.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','name_overrides','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_overrides','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_overrides','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_overrides','domain_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_overrides','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_overrides','domain_id',NULL,'');

-- COLUMN name_overrides.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','name_overrides','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_overrides','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_overrides','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_overrides','entity_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_overrides','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_overrides','entity_id',NULL,'');

-- COLUMN name_overrides.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','name_overrides','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_overrides','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_overrides','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_overrides','property_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_overrides','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_overrides','property_id',NULL,'');

-- COLUMN name_overrides.name

SELECT tap.has_column('_caosdb_schema_unit_tests','name_overrides','name','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','name_overrides','name','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','name_overrides','name','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','name_overrides','name','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','name_overrides','name','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','name_overrides','name','utf8mb3_unicode_ci','');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','name_overrides','`name_ov_dom_ent_idx`','');

-- INDEX name_overrides.name_ov_dom_ent_idx

SELECT tap.has_index('_caosdb_schema_unit_tests','name_overrides','name_ov_dom_ent_idx','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','name_overrides','name_ov_dom_ent_idx','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','name_overrides','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','name_overrides','`name_ov_ukey`,`name_ov_forkey_dom`,`name_ov_forkey_ent`,`name_ov_forkey_pro`','');

-- CONSTRAINT name_overrides.name_ov_forkey_dom

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_dom','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_dom','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_dom','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_dom','RESTRICT','');

-- CONSTRAINT name_overrides.name_ov_forkey_ent

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_ent','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_ent','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_ent','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_ent','RESTRICT','');

-- CONSTRAINT name_overrides.name_ov_forkey_pro

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_pro','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_pro','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_pro','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','name_overrides','name_ov_forkey_pro','RESTRICT','');

-- CONSTRAINT name_overrides.name_ov_ukey

SELECT tap.has_constraint('_caosdb_schema_unit_tests','name_overrides','name_ov_ukey','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','name_overrides','name_ov_ukey','UNIQUE','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.null_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','null_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','null_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','null_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','null_data','`domain_id`,`entity_id`,`property_id`,`status`,`pidx`','');

-- COLUMN null_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','null_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','null_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','null_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','null_data','domain_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','null_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','null_data','domain_id',NULL,'');

-- COLUMN null_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','null_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','null_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','null_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','null_data','entity_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','null_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','null_data','entity_id',NULL,'');

-- COLUMN null_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','null_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','null_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','null_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','null_data','property_id','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','null_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','null_data','property_id',NULL,'');

-- COLUMN null_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','null_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','null_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','null_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','null_data','status','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','null_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','null_data','status','utf8mb3_unicode_ci','');

-- COLUMN null_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','null_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','null_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','null_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','null_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','null_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','null_data','pidx',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','null_data','`null_data_dom_ent_idx`','');

-- INDEX null_data.null_data_dom_ent_idx

SELECT tap.has_index('_caosdb_schema_unit_tests','null_data','null_data_dom_ent_idx','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','null_data','null_data_dom_ent_idx','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','null_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','null_data','`null_forkey_dom`,`null_forkey_ent`,`null_forkey_pro`','');

-- CONSTRAINT null_data.null_forkey_dom

SELECT tap.has_constraint('_caosdb_schema_unit_tests','null_data','null_forkey_dom','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','null_data','null_forkey_dom','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','null_data','null_forkey_dom','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','null_data','null_forkey_dom','RESTRICT','');

-- CONSTRAINT null_data.null_forkey_ent

SELECT tap.has_constraint('_caosdb_schema_unit_tests','null_data','null_forkey_ent','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','null_data','null_forkey_ent','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','null_data','null_forkey_ent','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','null_data','null_forkey_ent','RESTRICT','');

-- CONSTRAINT null_data.null_forkey_pro

SELECT tap.has_constraint('_caosdb_schema_unit_tests','null_data','null_forkey_pro','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','null_data','null_forkey_pro','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','null_data','null_forkey_pro','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','null_data','null_forkey_pro','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.passwd
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','passwd','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','passwd','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','passwd','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','passwd','`principal`,`hash`,`alg`,`it`,`salt`','');

-- COLUMN passwd.principal

SELECT tap.has_column('_caosdb_schema_unit_tests','passwd','principal','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','passwd','principal','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','passwd','principal','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','passwd','principal',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','passwd','principal',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','passwd','principal',NULL,'');

-- COLUMN passwd.hash

SELECT tap.has_column('_caosdb_schema_unit_tests','passwd','hash','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','passwd','hash','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','passwd','hash','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','passwd','hash',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','passwd','hash',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','passwd','hash',NULL,'');

-- COLUMN passwd.alg

SELECT tap.has_column('_caosdb_schema_unit_tests','passwd','alg','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','passwd','alg','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','passwd','alg','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','passwd','alg','\'SHA-512\'','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','passwd','alg','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','passwd','alg','utf8mb3_unicode_ci','');

-- COLUMN passwd.it

SELECT tap.has_column('_caosdb_schema_unit_tests','passwd','it','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','passwd','it','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','passwd','it','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','passwd','it',5000,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','passwd','it',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','passwd','it',NULL,'');

-- COLUMN passwd.salt

SELECT tap.has_column('_caosdb_schema_unit_tests','passwd','salt','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','passwd','salt','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','passwd','salt','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','passwd','salt',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','passwd','salt',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','passwd','salt',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','passwd','`PRIMARY`','');

-- CONSTRAINT passwd.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','passwd','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','passwd','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','passwd','`principal`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.permissions
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','permissions','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','permissions','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','permissions','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','permissions','`role`,`permissions`','');

-- COLUMN permissions.role

SELECT tap.has_column('_caosdb_schema_unit_tests','permissions','role','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','permissions','role','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','permissions','role','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','permissions','role',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','permissions','role',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','permissions','role',NULL,'');

-- COLUMN permissions.permissions

SELECT tap.has_column('_caosdb_schema_unit_tests','permissions','permissions','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','permissions','permissions','mediumtext','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','permissions','permissions','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','permissions','permissions',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','permissions','permissions','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','permissions','permissions','utf8mb3_unicode_ci','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','permissions','`PRIMARY`,`perm_name_roles`','');

-- CONSTRAINT permissions.perm_name_roles

SELECT tap.has_constraint('_caosdb_schema_unit_tests','permissions','perm_name_roles','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','permissions','perm_name_roles','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','permissions','perm_name_roles','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','permissions','perm_name_roles','RESTRICT','');

-- CONSTRAINT permissions.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','permissions','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','permissions','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','permissions','`role`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.query_template_def
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','query_template_def','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','query_template_def','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','query_template_def','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','query_template_def','`id`,`definition`','');

-- COLUMN query_template_def.id

SELECT tap.has_column('_caosdb_schema_unit_tests','query_template_def','id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','query_template_def','id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','query_template_def','id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','query_template_def','id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','query_template_def','id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','query_template_def','id',NULL,'');

-- COLUMN query_template_def.definition

SELECT tap.has_column('_caosdb_schema_unit_tests','query_template_def','definition','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','query_template_def','definition','mediumtext','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','query_template_def','definition','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','query_template_def','definition',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','query_template_def','definition','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','query_template_def','definition','utf8mb3_unicode_ci','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','query_template_def','`PRIMARY`,`query_template_def_ibfk_1`','');

-- CONSTRAINT query_template_def.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','query_template_def','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','query_template_def','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','query_template_def','`id`','');

-- CONSTRAINT query_template_def.query_template_def_ibfk_1

SELECT tap.has_constraint('_caosdb_schema_unit_tests','query_template_def','query_template_def_ibfk_1','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','query_template_def','query_template_def_ibfk_1','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','query_template_def','query_template_def_ibfk_1','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','query_template_def','query_template_def_ibfk_1','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.reference_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','reference_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','reference_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','reference_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','reference_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`,`value_iversion`','');

-- COLUMN reference_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','domain_id',NULL,'');

-- COLUMN reference_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','entity_id',NULL,'');

-- COLUMN reference_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','property_id',NULL,'');

-- COLUMN reference_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','value','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','value',NULL,'');

-- COLUMN reference_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\',\'REPLACEMENT\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','status',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','status','utf8mb3_unicode_ci','');

-- COLUMN reference_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','pidx',NULL,'');

-- COLUMN reference_data.value_iversion

SELECT tap.has_column('_caosdb_schema_unit_tests','reference_data','value_iversion','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','reference_data','value_iversion','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','reference_data','value_iversion','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','reference_data','value_iversion','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','reference_data','value_iversion',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','reference_data','value_iversion',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','reference_data','`entity_id`,`value`','');

-- INDEX reference_data.entity_id

SELECT tap.has_index('_caosdb_schema_unit_tests','reference_data','entity_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','reference_data','entity_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','reference_data','`entity_id`,`property_id`','');

-- INDEX reference_data.value

SELECT tap.has_index('_caosdb_schema_unit_tests','reference_data','value','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','reference_data','value','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','reference_data','`value`,`value_iversion`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','reference_data','`ref_domain_id_entity`,`ref_entity_id_entity`,`ref_property_id_entity`,`ref_value_entity`,`reference_data_ibfk_1`','');

-- CONSTRAINT reference_data.ref_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','reference_data','ref_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','reference_data','ref_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','reference_data','ref_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','reference_data','ref_domain_id_entity','RESTRICT','');

-- CONSTRAINT reference_data.ref_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','reference_data','ref_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','reference_data','ref_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','reference_data','ref_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','reference_data','ref_entity_id_entity','RESTRICT','');

-- CONSTRAINT reference_data.ref_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','reference_data','ref_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','reference_data','ref_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','reference_data','ref_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','reference_data','ref_property_id_entity','RESTRICT','');

-- CONSTRAINT reference_data.ref_value_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','reference_data','ref_value_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','reference_data','ref_value_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','reference_data','ref_value_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','reference_data','ref_value_entity','RESTRICT','');

-- CONSTRAINT reference_data_ibfk_1
SELECT tap.has_constraint('_caosdb_schema_unit_tests','reference_data','reference_data_ibfk_1','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','reference_data','reference_data_ibfk_1','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','reference_data','reference_data_ibfk_1','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','reference_data','reference_data_ibfk_1','RESTRICT','');


-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.roles
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','roles','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','roles','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','roles','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','roles','`name`,`description`','');

-- COLUMN roles.name

SELECT tap.has_column('_caosdb_schema_unit_tests','roles','name','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','roles','name','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','roles','name','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','roles','name',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','roles','name',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','roles','name',NULL,'');

-- COLUMN roles.description

SELECT tap.has_column('_caosdb_schema_unit_tests','roles','description','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','roles','description','mediumtext','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','roles','description','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','roles','description','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','roles','description','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','roles','description','utf8mb3_unicode_ci','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','roles','`PRIMARY`','');

-- CONSTRAINT roles.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','roles','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','roles','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','roles','`name`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.stats
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','stats','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','stats','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','stats','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','stats','`name`,`value`','');

-- COLUMN stats.name

SELECT tap.has_column('_caosdb_schema_unit_tests','stats','name','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','stats','name','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','stats','name','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','stats','name',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','stats','name','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','stats','name','utf8mb3_unicode_ci','');

-- COLUMN stats.value

SELECT tap.has_column('_caosdb_schema_unit_tests','stats','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','stats','value','blob','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','stats','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','stats','value','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','stats','value',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','stats','value',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','stats','`PRIMARY`','');

-- CONSTRAINT stats.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','stats','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','stats','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','stats','`name`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.text_data
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','text_data','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','text_data','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','text_data','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','text_data','`domain_id`,`entity_id`,`property_id`,`value`,`status`,`pidx`','');

-- COLUMN text_data.domain_id

SELECT tap.has_column('_caosdb_schema_unit_tests','text_data','domain_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','text_data','domain_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','text_data','domain_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','text_data','domain_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','text_data','domain_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','text_data','domain_id',NULL,'');

-- COLUMN text_data.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','text_data','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','text_data','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','text_data','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','text_data','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','text_data','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','text_data','entity_id',NULL,'');

-- COLUMN text_data.property_id

SELECT tap.has_column('_caosdb_schema_unit_tests','text_data','property_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','text_data','property_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','text_data','property_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','text_data','property_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','text_data','property_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','text_data','property_id',NULL,'');

-- COLUMN text_data.value

SELECT tap.has_column('_caosdb_schema_unit_tests','text_data','value','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','text_data','value','text','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','text_data','value','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','text_data','value',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','text_data','value','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','text_data','value','utf8mb3_unicode_ci','');

-- COLUMN text_data.status

SELECT tap.has_column('_caosdb_schema_unit_tests','text_data','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','text_data','status','enum(\'OBLIGATORY\',\'RECOMMENDED\',\'SUGGESTED\',\'FIX\',\'REPLACEMENT\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','text_data','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','text_data','status',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','text_data','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','text_data','status','utf8mb3_unicode_ci','');

-- COLUMN text_data.pidx

SELECT tap.has_column('_caosdb_schema_unit_tests','text_data','pidx','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','text_data','pidx','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','text_data','pidx','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','text_data','pidx',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','text_data','pidx',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','text_data','pidx',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','text_data','`domain_id`','');

-- INDEX text_data.domain_id

SELECT tap.has_index('_caosdb_schema_unit_tests','text_data','domain_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','text_data','domain_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','text_data','`domain_id`,`entity_id`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','text_data','`str_domain_id_entity`,`str_entity_id_entity`,`str_property_id_entity`','');

-- CONSTRAINT text_data.str_domain_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','text_data','str_domain_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','text_data','str_domain_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','text_data','str_domain_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','text_data','str_domain_id_entity','RESTRICT','');

-- CONSTRAINT text_data.str_entity_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','text_data','str_entity_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','text_data','str_entity_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','text_data','str_entity_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','text_data','str_entity_id_entity','RESTRICT','');

-- CONSTRAINT text_data.str_property_id_entity

SELECT tap.has_constraint('_caosdb_schema_unit_tests','text_data','str_property_id_entity','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','text_data','str_property_id_entity','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','text_data','str_property_id_entity','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','text_data','str_property_id_entity','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.transaction_log
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','transaction_log','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','transaction_log','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','transaction_log','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','transaction_log','`transaction`,`entity_id`,`username`,`seconds`,`nanos`,`realm`','');

-- COLUMN transaction_log.transaction

SELECT tap.has_column('_caosdb_schema_unit_tests','transaction_log','transaction','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','transaction_log','transaction','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','transaction_log','transaction','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','transaction_log','transaction',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','transaction_log','transaction','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','transaction_log','transaction','utf8mb3_unicode_ci','');

-- COLUMN transaction_log.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','transaction_log','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','transaction_log','entity_id','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','transaction_log','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','transaction_log','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','transaction_log','entity_id','utf8mb4','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','transaction_log','entity_id','utf8mb4_bin','');

-- COLUMN transaction_log.username

SELECT tap.has_column('_caosdb_schema_unit_tests','transaction_log','username','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','transaction_log','username','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','transaction_log','username','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','transaction_log','username',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','transaction_log','username',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','transaction_log','username',NULL,'');

-- COLUMN transaction_log.seconds

SELECT tap.has_column('_caosdb_schema_unit_tests','transaction_log','seconds','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','transaction_log','seconds','bigint(20) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','transaction_log','seconds','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','transaction_log','seconds',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','transaction_log','seconds',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','transaction_log','seconds',NULL,'');

-- COLUMN transaction_log.nanos

SELECT tap.has_column('_caosdb_schema_unit_tests','transaction_log','nanos','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','transaction_log','nanos','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','transaction_log','nanos','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','transaction_log','nanos',0,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','transaction_log','nanos',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','transaction_log','nanos',NULL,'');

-- COLUMN transaction_log.realm

SELECT tap.has_column('_caosdb_schema_unit_tests','transaction_log','realm','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','transaction_log','realm','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','transaction_log','realm','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','transaction_log','realm',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','transaction_log','realm',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','transaction_log','realm',NULL,'');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','transaction_log','`entity_id`','');

-- INDEX transaction_log.entity_id

SELECT tap.has_index('_caosdb_schema_unit_tests','transaction_log','entity_id','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','transaction_log','entity_id','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','transaction_log','`entity_id`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.units_lin_con
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','units_lin_con','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','units_lin_con','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','units_lin_con','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','units_lin_con','`signature_from`,`signature_to`,`a`,`b_dividend`,`b_divisor`,`c`','');

-- COLUMN units_lin_con.signature_from

SELECT tap.has_column('_caosdb_schema_unit_tests','units_lin_con','signature_from','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','units_lin_con','signature_from','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','units_lin_con','signature_from','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','units_lin_con','signature_from',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','units_lin_con','signature_from',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','units_lin_con','signature_from',NULL,'');

-- COLUMN units_lin_con.signature_to

SELECT tap.has_column('_caosdb_schema_unit_tests','units_lin_con','signature_to','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','units_lin_con','signature_to','bigint(20)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','units_lin_con','signature_to','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','units_lin_con','signature_to',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','units_lin_con','signature_to',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','units_lin_con','signature_to',NULL,'');

-- COLUMN units_lin_con.a

SELECT tap.has_column('_caosdb_schema_unit_tests','units_lin_con','a','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','units_lin_con','a','decimal(65,30)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','units_lin_con','a','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','units_lin_con','a',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','units_lin_con','a',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','units_lin_con','a',NULL,'');

-- COLUMN units_lin_con.b_dividend

SELECT tap.has_column('_caosdb_schema_unit_tests','units_lin_con','b_dividend','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','units_lin_con','b_dividend','int(11)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','units_lin_con','b_dividend','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','units_lin_con','b_dividend',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','units_lin_con','b_dividend',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','units_lin_con','b_dividend',NULL,'');

-- COLUMN units_lin_con.b_divisor

SELECT tap.has_column('_caosdb_schema_unit_tests','units_lin_con','b_divisor','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','units_lin_con','b_divisor','int(11)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','units_lin_con','b_divisor','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','units_lin_con','b_divisor',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','units_lin_con','b_divisor',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','units_lin_con','b_divisor',NULL,'');

-- COLUMN units_lin_con.c

SELECT tap.has_column('_caosdb_schema_unit_tests','units_lin_con','c','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','units_lin_con','c','decimal(65,30)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','units_lin_con','c','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','units_lin_con','c',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','units_lin_con','c',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','units_lin_con','c',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','units_lin_con','`PRIMARY`','');

-- CONSTRAINT units_lin_con.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','units_lin_con','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','units_lin_con','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','units_lin_con','`signature_from`','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.user_info
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','user_info','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','user_info','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','user_info','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','user_info','`realm`,`name`,`email`,`status`,`entity`','');

-- COLUMN user_info.realm

SELECT tap.has_column('_caosdb_schema_unit_tests','user_info','realm','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_info','realm','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_info','realm','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_info','realm',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_info','realm',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_info','realm',NULL,'');

-- COLUMN user_info.name

SELECT tap.has_column('_caosdb_schema_unit_tests','user_info','name','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_info','name','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_info','name','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_info','name',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_info','name',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_info','name',NULL,'');

-- COLUMN user_info.email

SELECT tap.has_column('_caosdb_schema_unit_tests','user_info','email','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_info','email','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_info','email','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_info','email','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_info','email',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_info','email',NULL,'');

-- COLUMN user_info.status

SELECT tap.has_column('_caosdb_schema_unit_tests','user_info','status','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_info','status','enum(\'ACTIVE\',\'INACTIVE\')','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_info','status','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_info','status','\'INACTIVE\'','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_info','status','utf8mb3','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_info','status','utf8mb3_unicode_ci','');

-- COLUMN user_info.entity

SELECT tap.has_column('_caosdb_schema_unit_tests','user_info','entity','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_info','entity','varchar(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_info','entity','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_info','entity','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_info','entity','utf8mb4','');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_info','entity','utf8mb4_bin','');

-- INDEXES
SELECT tap.indexes_are('_caosdb_schema_unit_tests','user_info','`subject_entity`','');

-- INDEX user_info.subject_entity

SELECT tap.has_index('_caosdb_schema_unit_tests','user_info','subject_entity','');
SELECT tap.index_is_type('_caosdb_schema_unit_tests','user_info','subject_entity','BTREE','');
SELECT tap.is_indexed('_caosdb_schema_unit_tests','user_info','`entity`','');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','user_info','`PRIMARY`,`subjects_ibfk_2`','');

-- CONSTRAINT user_info.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','user_info','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','user_info','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','user_info','`realm`,`name`','');

-- CONSTRAINT user_info.subjects_ibfk_1

SELECT tap.has_constraint('_caosdb_schema_unit_tests','user_info','subjects_ibfk_2','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','user_info','subjects_ibfk_2','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','user_info','subjects_ibfk_2','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','user_info','subjects_ibfk_2','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.user_roles
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','user_roles','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','user_roles','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','user_roles','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','user_roles','`realm`,`user`,`role`','');

-- COLUMN user_roles.realm

SELECT tap.has_column('_caosdb_schema_unit_tests','user_roles','realm','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_roles','realm','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_roles','realm','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_roles','realm',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_roles','realm',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_roles','realm',NULL,'');

-- COLUMN user_roles.user

SELECT tap.has_column('_caosdb_schema_unit_tests','user_roles','user','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_roles','user','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_roles','user','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_roles','user',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_roles','user',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_roles','user',NULL,'');

-- COLUMN user_roles.role

SELECT tap.has_column('_caosdb_schema_unit_tests','user_roles','role','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','user_roles','role','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','user_roles','role','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','user_roles','role',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','user_roles','role',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','user_roles','role',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','user_roles','`PRIMARY`,`user_roles_ibfk_1`','');

-- CONSTRAINT user_roles.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','user_roles','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','user_roles','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','user_roles','`realm`,`user`,`role`','');

-- CONSTRAINT user_roles.user_roles_ibfk_1

SELECT tap.has_constraint('_caosdb_schema_unit_tests','user_roles','user_roles_ibfk_1','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','user_roles','user_roles_ibfk_1','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','user_roles','user_roles_ibfk_1','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','user_roles','user_roles_ibfk_1','RESTRICT','');

-- ***************************************************************
-- TABLE _caosdb_schema_unit_tests.entity_version
-- ***************************************************************

SELECT tap.has_table('_caosdb_schema_unit_tests','entity_version','');
SELECT tap.table_collation_is('_caosdb_schema_unit_tests','entity_version','utf8mb3_unicode_ci','');
SELECT tap.table_engine_is('_caosdb_schema_unit_tests','entity_version','InnoDB','');

-- COLUMNS
SELECT tap.columns_are('_caosdb_schema_unit_tests','entity_version','`entity_id`,`hash`,`version`,`_iversion`,`_ipparent`,`srid`','');

-- COLUMN entity_version.entity_id

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_version','entity_id','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_version','entity_id','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_version','entity_id','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_version','entity_id',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_version','entity_id',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_version','entity_id',NULL,'');

-- COLUMN entity_version.hash

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_version','hash','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_version','hash','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_version','hash','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_version','hash','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_version','hash',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_version','hash',NULL,'');

-- COLUMN entity_version.version

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_version','version','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_version','version','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_version','version','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_version','version',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_version','version',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_version','version',NULL,'');

-- COLUMN entity_version._iversion

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_version','_iversion','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_version','_iversion','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_version','_iversion','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_version','_iversion',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_version','_iversion',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_version','_iversion',NULL,'');

-- COLUMN entity_version._ipparent

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_version','_ipparent','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_version','_ipparent','int(10) unsigned','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_version','_ipparent','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_version','_ipparent','NULL','');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_version','_ipparent',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_version','_ipparent',NULL,'');

-- COLUMN entity_version.srid

SELECT tap.has_column('_caosdb_schema_unit_tests','entity_version','srid','');
SELECT tap.col_column_type_is('_caosdb_schema_unit_tests','entity_version','srid','varbinary(255)','');
SELECT tap.col_extra_is('_caosdb_schema_unit_tests','entity_version','srid','','');
SELECT tap.col_default_is('_caosdb_schema_unit_tests','entity_version','srid',NULL,'');
SELECT tap.col_charset_is('_caosdb_schema_unit_tests','entity_version','srid',NULL,'');
SELECT tap.col_collation_is('_caosdb_schema_unit_tests','entity_version','srid',NULL,'');

-- CONSTRAINTS
SELECT tap.constraints_are('_caosdb_schema_unit_tests','entity_version','`entity_version-e-v`,`PRIMARY`,`entity_version_ibfk_1`,`entity_version_ibfk_2`','');

-- CONSTRAINT entity_version.entity_id

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entity_version','entity_version-e-v','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entity_version','entity_version-e-v','UNIQUE','');

-- CONSTRAINT entity_version.PRIMARY

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entity_version','PRIMARY','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entity_version','PRIMARY','PRIMARY KEY','');
SELECT tap.col_is_pk('_caosdb_schema_unit_tests','entity_version','`entity_id`,`_iversion`','');

-- CONSTRAINT entity_version.entity_version_ibfk_1

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_1','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_1','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_1','CASCADE','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_1','RESTRICT','');

-- CONSTRAINT entity_version.entity_version_ibfk_2

SELECT tap.has_constraint('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_2','');
SELECT tap.constraint_type_is('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_2','FOREIGN KEY','');
SELECT tap.fk_on_delete('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_2','RESTRICT','');
SELECT tap.fk_on_update('_caosdb_schema_unit_tests','entity_version','entity_version_ibfk_2','RESTRICT','');


-- ***************************************************************
-- FUNCTIONS 
-- ***************************************************************

-- FUNCTION _caosdb_schema_unit_tests.CaosDBVersion

SELECT tap.has_function('_caosdb_schema_unit_tests','CaosDBVersion','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','CaosDBVersion','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','CaosDBVersion','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','CaosDBVersion','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','CaosDBVersion','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.convert_unit

SELECT tap.has_function('_caosdb_schema_unit_tests','convert_unit','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','convert_unit','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','convert_unit','decimal','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','convert_unit','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','convert_unit','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.constructDateTimeWhereClauseForColumn

SELECT tap.has_function('_caosdb_schema_unit_tests','constructDateTimeWhereClauseForColumn','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','constructDateTimeWhereClauseForColumn','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','constructDateTimeWhereClauseForColumn','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','constructDateTimeWhereClauseForColumn','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','constructDateTimeWhereClauseForColumn','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.getAggValueWhereClause

SELECT tap.has_function('_caosdb_schema_unit_tests','getAggValueWhereClause','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','getAggValueWhereClause','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','getAggValueWhereClause','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','getAggValueWhereClause','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','getAggValueWhereClause','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.getDateTimeWhereClause

SELECT tap.has_function('_caosdb_schema_unit_tests','getDateTimeWhereClause','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','getDateTimeWhereClause','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','getDateTimeWhereClause','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','getDateTimeWhereClause','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','getDateTimeWhereClause','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.getDoubleWhereClause

SELECT tap.has_function('_caosdb_schema_unit_tests','getDoubleWhereClause','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','getDoubleWhereClause','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','getDoubleWhereClause','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','getDoubleWhereClause','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','getDoubleWhereClause','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.getDateWhereClause

SELECT tap.has_function('_caosdb_schema_unit_tests','getDateWhereClause','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','getDateWhereClause','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','getDateWhereClause','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','getDateWhereClause','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','getDateWhereClause','CONTAINS SQL','');

-- FUNCTION _caosdb_schema_unit_tests.makeStmt

SELECT tap.has_function('_caosdb_schema_unit_tests','makeStmt','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','makeStmt','NO','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','makeStmt','varchar','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','makeStmt','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','makeStmt','NO SQL','');

-- FUNCTION _caosdb_schema_unit_tests.standard_unit

SELECT tap.has_function('_caosdb_schema_unit_tests','standard_unit','');
SELECT tap.function_is_deterministic('_caosdb_schema_unit_tests','standard_unit','YES','');
SELECT tap.function_data_type_is('_caosdb_schema_unit_tests','standard_unit','bigint','');
SELECT tap.function_security_type_is('_caosdb_schema_unit_tests','standard_unit','DEFINER','');
SELECT tap.function_sql_data_access_is('_caosdb_schema_unit_tests','standard_unit','CONTAINS SQL','');

-- ***************************************************************
-- PROCEDURES 
-- ***************************************************************

-- PROCEDURES _caosdb_schema_unit_tests.applyBackReference

SELECT tap.has_procedure('_caosdb_schema_unit_tests','applyBackReference','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','applyBackReference','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','applyBackReference','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','applyBackReference','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.applyPOV

SELECT tap.has_procedure('_caosdb_schema_unit_tests','applyPOV','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','applyPOV','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','applyPOV','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','applyPOV','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.applyRefPOV

SELECT tap.has_procedure('_caosdb_schema_unit_tests','applyRefPOV','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','applyRefPOV','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','applyRefPOV','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','applyRefPOV','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.applyIDFilter

SELECT tap.has_procedure('_caosdb_schema_unit_tests','applyIDFilter','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','applyIDFilter','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','applyIDFilter','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','applyIDFilter','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.applySAT

SELECT tap.has_procedure('_caosdb_schema_unit_tests','applySAT','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','applySAT','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','applySAT','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','applySAT','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.applyTransactionFilter

SELECT tap.has_procedure('_caosdb_schema_unit_tests','applyTransactionFilter','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','applyTransactionFilter','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','applyTransactionFilter','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','applyTransactionFilter','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.calcComplementUnion

SELECT tap.has_procedure('_caosdb_schema_unit_tests','calcComplementUnion','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','calcComplementUnion','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','calcComplementUnion','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','calcComplementUnion','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.calcDifference

SELECT tap.has_procedure('_caosdb_schema_unit_tests','calcDifference','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','calcDifference','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','calcDifference','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','calcDifference','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.calcIntersection

SELECT tap.has_procedure('_caosdb_schema_unit_tests','calcIntersection','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','calcIntersection','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','calcIntersection','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','calcIntersection','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.calcUnion

SELECT tap.has_procedure('_caosdb_schema_unit_tests','calcUnion','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','calcUnion','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','calcUnion','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','calcUnion','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.cleanUpLinCon

SELECT tap.has_procedure('_caosdb_schema_unit_tests','cleanUpLinCon','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','cleanUpLinCon','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','cleanUpLinCon','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','cleanUpLinCon','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.cleanUpQuery

SELECT tap.has_procedure('_caosdb_schema_unit_tests','cleanUpQuery','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','cleanUpQuery','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','cleanUpQuery','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','cleanUpQuery','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.copyTable

SELECT tap.has_procedure('_caosdb_schema_unit_tests','copyTable','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','copyTable','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','copyTable','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','copyTable','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.createTmpTable

SELECT tap.has_procedure('_caosdb_schema_unit_tests','createTmpTable','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','createTmpTable','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','createTmpTable','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','createTmpTable','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.createTmpTable2

SELECT tap.has_procedure('_caosdb_schema_unit_tests','createTmpTable2','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','createTmpTable2','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','createTmpTable2','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','createTmpTable2','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.deleteEntity

SELECT tap.has_procedure('_caosdb_schema_unit_tests','deleteEntity','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','deleteEntity','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','deleteEntity','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','deleteEntity','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.deleteEntityProperties

SELECT tap.has_procedure('_caosdb_schema_unit_tests','deleteEntityProperties','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','deleteEntityProperties','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','deleteEntityProperties','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','deleteEntityProperties','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.deleteIsa

SELECT tap.has_procedure('_caosdb_schema_unit_tests','deleteIsa','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','deleteIsa','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','deleteIsa','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','deleteIsa','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.deleteLinCon

SELECT tap.has_procedure('_caosdb_schema_unit_tests','deleteLinCon','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','deleteLinCon','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','deleteLinCon','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','deleteLinCon','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.entityACL

SELECT tap.has_procedure('_caosdb_schema_unit_tests','entityACL','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','entityACL','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','entityACL','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','entityACL','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.finishSubProperty

SELECT tap.has_procedure('_caosdb_schema_unit_tests','finishSubProperty','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','finishSubProperty','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','finishSubProperty','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','finishSubProperty','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.getChildren

SELECT tap.has_procedure('_caosdb_schema_unit_tests','getChildren','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','getChildren','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','getChildren','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','getChildren','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.getFileIdByPath

SELECT tap.has_procedure('_caosdb_schema_unit_tests','getFileIdByPath','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','getFileIdByPath','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','getFileIdByPath','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','getFileIdByPath','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initBackReference

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initBackReference','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initBackReference','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initBackReference','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initBackReference','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initDisjunctionFilter

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initDisjunctionFilter','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initDisjunctionFilter','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initDisjunctionFilter','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initDisjunctionFilter','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initPOVRefidsTable

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initPOVRefidsTable','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initPOVRefidsTable','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initPOVRefidsTable','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initPOVRefidsTable','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initQuery

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initQuery','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initQuery','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initQuery','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initQuery','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.insertEntity

SELECT tap.has_procedure('_caosdb_schema_unit_tests','insertEntity','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','insertEntity','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','insertEntity','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','insertEntity','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.getDependentEntities

SELECT tap.has_procedure('_caosdb_schema_unit_tests','getDependentEntities','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','getDependentEntities','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','getDependentEntities','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','getDependentEntities','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initEmptyTargetSet

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initEmptyTargetSet','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initEmptyTargetSet','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initEmptyTargetSet','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initEmptyTargetSet','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initEntity

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initEntity','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initEntity','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initEntity','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initEntity','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.insertLinCon

SELECT tap.has_procedure('_caosdb_schema_unit_tests','insertLinCon','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','insertLinCon','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','insertLinCon','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','insertLinCon','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.intersectTable

SELECT tap.has_procedure('_caosdb_schema_unit_tests','intersectTable','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','intersectTable','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','intersectTable','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','intersectTable','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initPOVPropertiesTable

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initPOVPropertiesTable','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initPOVPropertiesTable','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initPOVPropertiesTable','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initPOVPropertiesTable','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initSubEntity

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initSubEntity','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initSubEntity','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initSubEntity','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initSubEntity','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.isSubtype

SELECT tap.has_procedure('_caosdb_schema_unit_tests','isSubtype','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','isSubtype','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','isSubtype','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','isSubtype','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.overrideDesc

SELECT tap.has_procedure('_caosdb_schema_unit_tests','overrideDesc','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','overrideDesc','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','overrideDesc','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','overrideDesc','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.overrideName

SELECT tap.has_procedure('_caosdb_schema_unit_tests','overrideName','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','overrideName','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','overrideName','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','overrideName','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.overrideType

SELECT tap.has_procedure('_caosdb_schema_unit_tests','overrideType','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','overrideType','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','overrideType','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','overrideType','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.raiseWarning

SELECT tap.has_procedure('_caosdb_schema_unit_tests','raiseWarning','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','raiseWarning','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','raiseWarning','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','raiseWarning','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.registerReplacementIds

SELECT tap.has_procedure('_caosdb_schema_unit_tests','registerReplacementIds','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','registerReplacementIds','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','registerReplacementIds','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','registerReplacementIds','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.initSubProperty

SELECT tap.has_procedure('_caosdb_schema_unit_tests','initSubProperty','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','initSubProperty','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','initSubProperty','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','initSubProperty','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.insertEntityProperty

SELECT tap.has_procedure('_caosdb_schema_unit_tests','insertEntityProperty','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','insertEntityProperty','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','insertEntityProperty','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','insertEntityProperty','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.registerTempTableName

SELECT tap.has_procedure('_caosdb_schema_unit_tests','registerTempTableName','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','registerTempTableName','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','registerTempTableName','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','registerTempTableName','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.retrieveEntityParents

SELECT tap.has_procedure('_caosdb_schema_unit_tests','retrieveEntityParents','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','retrieveEntityParents','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','retrieveEntityParents','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','retrieveEntityParents','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.insertIsa

SELECT tap.has_procedure('_caosdb_schema_unit_tests','insertIsa','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','insertIsa','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','insertIsa','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','insertIsa','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.retrieveEntity

SELECT tap.has_procedure('_caosdb_schema_unit_tests','retrieveEntity','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','retrieveEntity','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','retrieveEntity','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','retrieveEntity','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.retrieveEntityProperties

SELECT tap.has_procedure('_caosdb_schema_unit_tests','retrieveEntityProperties','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','retrieveEntityProperties','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','retrieveEntityProperties','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','retrieveEntityProperties','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.showEntityAutoIncr

SELECT tap.has_procedure('_caosdb_schema_unit_tests','showEntityAutoIncr','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','showEntityAutoIncr','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','showEntityAutoIncr','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','showEntityAutoIncr','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.updateLinCon

SELECT tap.has_procedure('_caosdb_schema_unit_tests','updateLinCon','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','updateLinCon','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','updateLinCon','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','updateLinCon','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.retrieveOverrides

SELECT tap.has_procedure('_caosdb_schema_unit_tests','retrieveOverrides','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','retrieveOverrides','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','retrieveOverrides','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','retrieveOverrides','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.updateEntity

SELECT tap.has_procedure('_caosdb_schema_unit_tests','updateEntity','');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','updateEntity','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','updateEntity','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','updateEntity','CONTAINS SQL','');

-- PROCEDURES _caosdb_schema_unit_tests.insert_single_child_version

SELECT tap.has_procedure('_caosdb_schema_unit_tests', 'insert_single_child_version', '');
SELECT tap.procedure_is_deterministic('_caosdb_schema_unit_tests','insert_single_child_version','NO','');
SELECT tap.procedure_security_type_is('_caosdb_schema_unit_tests','insert_single_child_version','DEFINER','');
SELECT tap.procedure_sql_data_access_is('_caosdb_schema_unit_tests','insert_single_child_version','CONTAINS SQL','');

CALL tap.finish();
ROLLBACK;
