#!/bin/bash
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2020 IndiScale <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

# Dump a database with all procedures, permissions, structure and data
set -e

if [ -z "$UTILSPATH" ]; then
    UTILSPATH="$(realpath $(dirname $(dirname $0)))/utils"
    export UTILSPATH
fi

MAX_ENTITIES=500
# Load settings from .config and defaults #####################################
. $UTILSPATH/load_settings.sh

# load useful functions #######################################################
. $UTILSPATH/helpers.sh

# Test mysql connection
echo -n 'testing connection... '
$UTILSPATH/make_db test-connection > /dev/null
echo '[Ok]'

echo -n 'size of database... '
# check size of database
num=$(mysql_execute "SELECT COUNT(*) FROM entities" | sed 's/COUNT(\*)//')
if [ $num -gt $MAX_ENTITIES ]; then
    failure "There are more than $MAX_ENTITIES entities... " \
            "Do not run this on a production instance!"
elif [ $num -lt 5 ]; then
    failure "There are less than 5 entities... " \
            "Please use this with a database with some example content!"
fi
echo '[Ok]'

###############################################################################
# Below we create a dump, then run tests, and create a dump again. Afterwards
# the dumps are checked if they are not empty and whether they differ.
# This should make sure that tests interfere somehow with the real database.
# This also provides a very basic test that sql dumps work.
###############################################################################

# creating a dump of current db
backupdir=$(realpath 'tmp_backup')
if [ -e "$backupdir" ]; then
    failure "Temporary directory $backupdir already exists. Please remove it"
fi
echo -n 'creating initial dump... '
BACKUPDIR=$backupdir $UTILSPATH/backup.sh > /dev/null
echo '[Ok]'
# run normal tests
echo -n 'running sql tests... '
$UTILSPATH/make_db test --fresh
echo '[Ok]'

echo -n 'creating secondary dump... '
BACKUPDIR=$backupdir $UTILSPATH/backup.sh > /dev/null
echo '[Ok]'

#compare dumps
pushd $backupdir > /dev/null
echo -n 'checking dumps... '
for filename in "./*.sql"; do
    # assures entities table is created, i.e. there is content
    cat $filename | grep "CREATE TABLE .entities." > /dev/null
    # remove time stamp
    sed -i '/Dump completed on/d' $filename
done
echo '[Ok]'

echo -n 'comparing dumps... '
if ! diff -q * > /dev/null ; then
    diff *
    failure "The second dump differs from the first one. This possibly means " \
            "that tests changed the real database."
fi
echo '[Ok]'
rm -r $backupdir
popd > /dev/null

###############################################################################
# Below we execute other util commands as a simple check that they do not fail.
###############################################################################
echo -n 'testing logging... '
$UTILSPATH/log.sh "start"
$UTILSPATH/log.sh "get" > /dev/null
$UTILSPATH/log.sh "stop"
$UTILSPATH/make_db grant-permission anonymous \
                   '[{"grant":"true","priority":"true","permission":"*"}]'
echo '[Ok]'
echo '[Completed]'
