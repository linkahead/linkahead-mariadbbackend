#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
# Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2020 IndiScale <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

###############################################################################
#                         Apply all available patches                         #
###############################################################################

set -e

if [ -z "$UTILSPATH" ]; then
    UTILSPATH=$(realpath "$(dirname $0)/../utils")
    export UTILSPATH
fi

source $UTILSPATH/load_settings.sh
source $UTILSPATH/helpers.sh

PATCHES="./patch*/patch.sh"

for p in $PATCHES
do
    $p "$@" --patch=$p
done

cd ../

$UTILSPATH/update_sql_procedures.sh
