#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# this patch adds a desc_override_table
# Update mysql schema from version v2.0.2 to version v2.0.3
NEW_VERSION="v2.0.3"
OLD_VERSION="v2.0.2"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

dump_table 'datetime_data'

function add_desc_override_table {
    mysql_execute 'CREATE TABLE IF NOT EXISTS desc_overrides (
	domain_id INT UNSIGNED,
	entity_id INT UNSIGNED,
	description VARCHAR(255),
	UNIQUE KEY (domain_id, entity_id),
	CONSTRAINT `descov_d_ent_id` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
	CONSTRAINT `descov_e_ent_id` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB;'
}

add_desc_override_table

update_version $NEW_VERSION
success
