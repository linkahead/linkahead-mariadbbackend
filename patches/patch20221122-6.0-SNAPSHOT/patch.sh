#!/bin/bash
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

# Update mysql schema to version v5.0.0
# Drop the 'rules' table.

NEW_VERSION="v6.0.0-SNAPSHOT-EXTIDS"
OLD_VERSION="v5.0.0"

DROP_TABLE_PASSWORDS='DROP TABLE IF EXISTS passwords;'

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

mysql_execute "$DROP_TABLE_PASSWORDS"
mysql_execute_file $PATCH_DIR/create_entity_ids_table.sql

update_version $NEW_VERSION


success

