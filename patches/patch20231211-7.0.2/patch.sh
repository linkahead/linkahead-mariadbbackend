#!/bin/bash
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

# Update mysql schema to version v7.0.2
# Patch the broken data_type table (fix id for file references as per
# https://gitlab.com/linkahead/linkahead-server/-/issues/246)

NEW_VERSION="v7.0.2"
OLD_VERSION="v7.0.1"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

mysql_execute "UPDATE data_type SET datatype=17 WHERE datatype=3"

update_version $NEW_VERSION

success
