#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# Create a new isa_cache table which does not only store a sparse graph of direct isa-relations but caches all subtype relations.
# Update mysql schema to version v2.0.30
NEW_VERSION="v2.0.30"
OLD_VERSION="v2.0.29"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


check_version $OLD_VERSION

# new table
mysql_execute 'DROP TABLE IF EXISTS isa_cache; CREATE TABLE isa_cache
    (child INT UNSIGNED NOT NULL, 
	 parent INT UNSIGNED NOT NULL, 
	 rpath VARCHAR(255) NOT NULL, 
	 PRIMARY KEY (`child`, `parent`, `rpath`),
	 CONSTRAINT `isa_cache_child_entity` FOREIGN KEY (`child`) REFERENCES `entities` (`id`),
	 CONSTRAINT `isa_cache_parent_entity` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`)
	 );'

#move old relations to new table
mysql_execute '
	DELIMITER //
	CREATE PROCEDURE moveStuff()
	BEGIN
			DECLARE i BIGINT UNSIGNED DEFAULT 0;
			DECLARE n BIGINT UNSIGNED DEFAULT NULL;
			DECLARE c BIGINT UNSIGNED DEFAULT NULL;
			DECLARE p BIGINT UNSIGNED DEFAULT NULL;

			SELECT COUNT(*) INTO n FROM isa;
			WHILE i<n DO
				SELECT child, parent INTO c, p FROM isa LIMIT i,1;
				
				INSERT INTO isa_cache (child, parent, rpath) VALUES (c,p,c);
		   
				INSERT IGNORE INTO isa_cache SELECT c AS child, i.parent AS parent, IF(p=i.rpath or i.rpath=parent, p, concat(p, ">", i.rpath)) AS rpath FROM isa_cache AS i WHERE i.child = p;
		   
		   
				INSERT IGNORE INTO isa_cache SELECT l.child, r.parent, if(l.rpath=l.child and r.rpath=c, c, concat(if(l.rpath=l.child,c,concat(l.rpath, "", c)), if(r.rpath=c,"",concat(">", r.rpath)))) AS rpath FROM isa_cache AS l INNER JOIN isa_cache AS r ON (l.parent = r.child AND l.parent=c);

			SET i = i + 1;
			END WHILE;
	END; //
	DELIMITER ;


	CALL moveStuff;
	DROP PROCEDURE moveStuff;
'

#drop old isa table
mysql_execute '
    DROP TABLE isa;
'

update_version $NEW_VERSION

success

