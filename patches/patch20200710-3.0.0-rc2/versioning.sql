/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

-- Create tables for versioning.
DROP TABLE IF EXISTS archive_reference_data;
DROP TABLE IF EXISTS archive_null_data;
DROP TABLE IF EXISTS archive_text_data;
DROP TABLE IF EXISTS archive_name_data;
DROP TABLE IF EXISTS archive_enum_data;
DROP TABLE IF EXISTS archive_integer_data;
DROP TABLE IF EXISTS archive_double_data;
DROP TABLE IF EXISTS archive_datetime_data;
DROP TABLE IF EXISTS archive_date_data;
DROP TABLE IF EXISTS archive_name_overrides;
DROP TABLE IF EXISTS archive_desc_overrides;
DROP TABLE IF EXISTS archive_data_type;
DROP TABLE IF EXISTS archive_collection_type;
DROP TABLE IF EXISTS archive_query_template_def;
DROP TABLE IF EXISTS archive_files;
DROP TABLE IF EXISTS archive_entities;
DROP TABLE IF EXISTS archive_isa;
DROP TABLE IF EXISTS entity_version;
DROP TABLE IF EXISTS transactions;

CREATE TABLE transactions (
  srid VARBINARY(255) PRIMARY KEY,  -- server request ID ?
  username VARBINARY(255) NOT NULL, -- who did the transactions: username
  realm VARBINARY(255) NOT NULL,    -- who did the transactions: realm
  seconds BIGINT UNSIGNED NOT NULL, -- time of transaction: seconds
  nanos INT(10) UNSIGNED NOT NULL   -- time of transaction: sub-second time resolution
) ENGINE=InnoDB;

-- TODO remove ON DELETE CASCADE when feature is stable.
CREATE TABLE entity_version (
  entity_id INT UNSIGNED NOT NULL,
  hash VARBINARY(255) DEFAULT NULL,
  version VARBINARY(255) NOT NULL, -- external version identifier, may be globally unique
  _iversion INT UNSIGNED NOT NULL, -- internal version ID, typically an incremental counter
  _ipparent INT UNSIGNED NULL,  -- (internal) ID of the primary parent, i.e. of the predecessor
  srid VARBINARY(255) NOT NULL,
  PRIMARY KEY (`entity_id`, `_iversion`),
  FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`srid`) REFERENCES `transactions` (`srid`),
  UNIQUE `entity_version-e-v` (`entity_id`, `version`)
) ENGINE=InnoDB;

ALTER TABLE reference_data
    ADD COLUMN `value_iversion` INT UNSIGNED DEFAULT NULL,
    ADD FOREIGN KEY (`value`, `value_iversion`) REFERENCES
        entity_version (`entity_id`, `_iversion`);


CREATE TABLE archive_isa (
  child INT UNSIGNED NOT NULL,
  child_iversion INT UNSIGNED NOT NULL,
  parent INT UNSIGNED NOT NULL,
  FOREIGN KEY (`parent`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`child`, `child_iversion`) REFERENCES `entity_version`
      (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_reference_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value INT UNSIGNED NOT NULL,
    value_iversion INT UNSIGNED DEFAULT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`value`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_null_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_text_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value TEXT NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_name_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value VARCHAR(255) NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    KEY (`value`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_enum_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value VARBINARY(255) NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_integer_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value BIGINT NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    unit_sig BIGINT DEFAULT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_double_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value DOUBLE NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    unit_sig BIGINT DEFAULT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_datetime_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value BIGINT NOT NULL,
    value_ns INT(10) UNSIGNED DEFAULT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_date_data (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    value INT(11) NOT NULL,
    status ENUM('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX')
        NOT NULL,
    pidx INT(10) UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_name_overrides (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    name VARCHAR(255) NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    UNIQUE KEY `archive_name_overrides-d-e-p-v` (`domain_id`, `entity_id`, `property_id`, `_iversion`),
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_desc_overrides (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    description TEXT NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    UNIQUE KEY `archive_desc_overrides-d-e-p-v` (`domain_id`, `entity_id`, `property_id`, `_iversion`),
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_data_type (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    datatype INT UNSIGNED NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    UNIQUE KEY `archive_data_type-d-e-p-v` (`domain_id`, `entity_id`, `property_id`, `_iversion`),
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_collection_type (
    domain_id INT UNSIGNED NOT NULL,
    entity_id INT UNSIGNED NOT NULL,
    property_id INT UNSIGNED NOT NULL,
    collection VARCHAR(255) NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    UNIQUE KEY `archive_collection_type-d-e-p-v` (`domain_id`, `entity_id`, `property_id`, `_iversion`),
    KEY (`domain_id`, `entity_id`, `_iversion`),
    KEY (`domain_id`, `_iversion`),
    FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_query_template_def (
    id INT UNSIGNED NOT NULL,
    definition MEDIUMTEXT NOT NULL,
    _iversion INT UNSIGNED NOT NULL,
    PRIMARY KEY (`id`, `_iversion`),
    FOREIGN KEY (`id`, `_iversion`)
        REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_files (
    file_id INT UNSIGNED NOT NULL,
    `path` TEXT NOT NULL,
    size BIGINT UNSIGNED NOT NULL,
    hash BINARY(64) DEFAULT NULL,
    _iversion INT UNSIGNED NOT NULL,
    PRIMARY KEY (`file_id`, `_iversion`),
    FOREIGN KEY (`file_id`, `_iversion`)
        REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE archive_entities (
    id INT UNSIGNED NOT NULL,
    description TEXT DEFAULT NULL,
    role ENUM('RECORDTYPE','RECORD','FILE','DOMAIN',
        'PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') NOT NULL,
    acl INT(10) UNSIGNED DEFAULT NULL,
    _iversion INT UNSIGNED NOT NULL,
    PRIMARY KEY (`id`, `_iversion`),
    FOREIGN KEY (`id`, `_iversion`)
        REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE,
    FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB;

ALTER TABLE collection_type ADD UNIQUE KEY `collection_type-d-e-p` (`domain_id`, `entity_id`, `property_id`);

INSERT INTO feature_config (_key, _value) VALUES ("ENTITY_VERSIONING", "ENABLED");
