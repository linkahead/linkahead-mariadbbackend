#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# adds a date_data table
# Update mysql schema to version v2.0.9
NEW_VERSION="v2.0.9"
OLD_VERSION="v2.0.8"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

function create_new_table {
	mysql_execute 'CREATE TABLE date_data (
		domain_id INT UNSIGNED,
		entity_id INT UNSIGNED,
		property_id INT UNSIGNED,
		value INT,
		status ENUM("OBLIGATORY","RECOMMENDED","SUGGESTED","FIX"),
		pidx TINYINT UNSIGNED,
		INDEX `date_data_dom_ent_idx` (domain_id,entity_id),
		FOREIGN KEY `date_ov_forkey_dom` (`domain_id`) REFERENCES `entities` (`id`),
		FOREIGN KEY `date_ov_forkey_ent` (`entity_id`) REFERENCES `entities` (`id`),
		FOREIGN KEY `date_ov_forkey_pro` (`property_id`) REFERENCES `entities` (`id`)
	) ENGINE=InnoDB;
	'
}

create_new_table
update_version $NEW_VERSION

success
