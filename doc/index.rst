
Welcome to documentation of CaosDB's MySQL Backend!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Getting started <README_SETUP>
   Concepts <concepts>
   Maintenance 
   API documentation <functions>
   Related Projects <related_projects/index>
   Back to Overview <https://docs.indiscale.com/>

This documentation helps you to :doc:`get started<README_SETUP>`, explains the most important
:doc:`concepts<concepts>` .


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
