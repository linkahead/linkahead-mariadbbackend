
Maintenance
===========


Creating a Backup
-----------------


You can use the Python script ``utils/backup.py`` to create a backup (SQL dump) 
of the SQL-Backend::

    ./utils/backup.py -d folder/where/the/backup/is/created

You can do this while CaosDB is online.

Restoring a Backup
------------------
CaosDB should be offline for restoring. 

You can use the Bash script ``utils/make_db`` to restore a backup (SQL dump) 
of the SQL-Backend::

    ./make_db restore_db path/to/sql.dump
