# Release Guidelines for the CaosDB MySQL Backend

This document specifies release guidelines in addition to the general release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb-meta/-/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing. (Note that pipelines may fail if the major
  version increased. Rely on manual tests in that case.)
* (FEATURES.md is up-to-date and a public API is being declared in that document.)
* CHANGELOG.md is up-to-date.
* (DEPENDENCIES.md is up-to-date.)
* Version in `doc/conf.py` is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Check all general prerequisites.

3. Assure that the latest patch actually updates the CaosDBVersion() procedure
   and that the version matches the version you are about to release.

4. Update version in `doc/conf.py` and in `CITATION.cff`.

5. Merge the release branch into the main branch.

6. Wait for the main branch pipeline to pass.

7. Tag the latest commit of the main branch with `v<VERSION>`.

8. Delete the release branch.

9. Merge the main branch back into the dev branch and add the `[UNRELEASED]`
   section to the CHANGELOG.

10. Create a release at the [gitlab repo](https://gitlab.com/linkahead/linkahead-mariadbbackend/-/releases)

11. If necessary, i.e., if the patch or major versions were increased
	with the release, do a LinkAhead server release.
