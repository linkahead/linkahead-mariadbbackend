# This file is a part of the Linkahead Project.
#
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2024 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

SHELL=/bin/bash

INSTALL_SQL_FILE=db_5_0.sql

# The hostname, used by testing in the CI pipeline
SQL_HOST ?= mysql

.PHONY: test-connection
test-connection:
	./utils/make_db test-connection

.PHONY: upgrade
upgrade:
	@cd patches; ./applyPatches.sh --env=../.config
	@./utils/make_db sanity_check

.PHONY: install
install: _install _grant upgrade

.PHONY: _install
_install:
	./utils/make_db install_db

.PHONY: _grant
_grant:
	./utils/make_db grant

# Drop the user and a given database
.PHONY: drop-%
drop-%:
	./utils/make_db drop $(patsubst drop-%,%,$@)

.PHONY: test
test:
	./utils/make_db test --fresh

.PHONY: test-dump-update
test-dump-update:
	pytest dump_updates

# Run tests with a database which is started in a MariaDB Docker container
.PHONY: test-docker
test-docker:
	@docker kill caosdb-mysqlserver-test || true
	@docker container rm -v caosdb-mysqlserver-test || true
	@docker run --name caosdb-mysqlserver-test -p "3306:3306" \
		-e MYSQL_ROOT_PASSWORD="pass-for-test" -d mariadb:10.11
	@sleep 10
	MAINPATH=$(realpath tests/docker_env) utils/make_db test --fresh
	make test-docker-stop

# if automatic stopping failed
.PHONY: test-docker-stop
test-docker-stop:
	docker kill caosdb-mysqlserver-test
	docker container rm -v caosdb-mysqlserver-test

# Compile the standalone documentation
.PHONY: doc
doc:
	$(MAKE) -C doc html

# Run tests in a Gitlab pipeline
.PHONY: pipeline-test
pipeline-test:
	cp config.defaults .config
	echo 'DATABASE_USER_HOST_LIST="%,"' >> .config
	echo "MYSQL_USER_PASSWORD=$(MYSQL_ROOT_PASSWORD)" >> .config
	echo "MYSQL_HOST=$(SQL_HOST)" >> .config
	echo "MYSQL_OPTS=--protocol=TCP" >> .config
	sleep 10
	make install
	./utils/make_db restore_db tests/example.dump.sql
	make upgrade
	./tests/test_utils.sh
