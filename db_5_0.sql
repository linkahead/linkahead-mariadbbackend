/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <www.indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#-- MySQL batch stack to create the CaosDB database structure, v5.0.0
#-- AUTHOR: Timm Fitschen <t.fitsche@indiscale.com>
#-- DATE: 2021-09-15





#-- *************
#-- Drop database
#-- *************

DROP DATABASE IF EXISTS db_5_0;

#-- ***************
#-- Create database
#-- ***************

CREATE DATABASE IF NOT EXISTS db_5_0 DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8_unicode_ci;
USE db_5_0;

#-- ***********************
#-- Create function version
#-- ***********************

CREATE FUNCTION CaosDBVersion() RETURNS VARCHAR(255) DETERMINISTIC
RETURN 'v5.0.0';

#-- *************
#-- Create tables
#-- *************

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `srid` varbinary(255) NOT NULL,
  `username` varbinary(255) NOT NULL,
  `realm` varbinary(255) NOT NULL,
  `seconds` bigint(20) unsigned NOT NULL,
  `nanos` int(10) unsigned NOT NULL,
  PRIMARY KEY (`srid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
INSERT INTO `transactions` VALUES ('cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e','administration','CaosDB',0,0);
UNLOCK TABLES;

--
-- Table structure for table `entity_acl`
--

DROP TABLE IF EXISTS `entity_acl`;
CREATE TABLE `entity_acl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acl` varbinary(65525) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_acl_acl` (`acl`(3072))
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entity_acl`
--

LOCK TABLES `entity_acl` WRITE;
INSERT INTO `entity_acl` (`acl`) VALUES ('');
UPDATE `entity_acl` SET id = 0;
UNLOCK TABLES;

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
CREATE TABLE `entities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier.',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` enum('RECORDTYPE','RECORD','FILE','DOMAIN','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') COLLATE utf8_unicode_ci NOT NULL,
  `acl` int(10) unsigned DEFAULT NULL COMMENT 'Access Control List for the entity.',
  PRIMARY KEY (`id`),
  KEY `entity_entity_acl` (`acl`),
  CONSTRAINT `entity_entity_acl` FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `entities` WRITE;
INSERT INTO `entities` VALUES (100,'The default domain.','ROLE',0),(1,'The default recordtype.','ROLE',0),(2,'The default record.','ROLE',0),(3,'The default file.','ROLE',0),(4,'The default property.','ROLE',0),(7,'The default datatype.','ROLE',0),(8,'The QueryTemplate role.','ROLE',0),(11,'The default reference data type.','DATATYPE',0),(12,'The default integer data type.','DATATYPE',0),(13,'The default double data type.','DATATYPE',0),(14,'The default text data type.','DATATYPE',0),(15,'The default datetime data type.','DATATYPE',0),(16,'The default timespan data type.','DATATYPE',0),(17,'The default file reference data type.','DATATYPE',0),(18,'The defaulf boolean data type','DATATYPE',0),(20,'Name of an entity','PROPERTY',0),(21,'Unit of an entity.','PROPERTY',0),(24,'Description of an entity.','PROPERTY',0),(50,'The SQLite file data type.','DATATYPE',0),(99,NULL,'RECORDTYPE',0);
UPDATE `entities` SET id = 0 WHERE id = 100;
UNLOCK TABLES;

--
-- Table structure for table `entity_version`
--

DROP TABLE IF EXISTS `entity_version`;
CREATE TABLE `entity_version` (
  `entity_id` int(10) unsigned NOT NULL,
  `hash` varbinary(255) DEFAULT NULL,
  `version` varbinary(255) NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `_ipparent` int(10) unsigned DEFAULT NULL,
  `srid` varbinary(255) NOT NULL,
  PRIMARY KEY (`entity_id`,`_iversion`),
  UNIQUE KEY `entity_version-e-v` (`entity_id`,`version`),
  KEY `srid` (`srid`),
  CONSTRAINT `entity_version_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `entity_version_ibfk_2` FOREIGN KEY (`srid`) REFERENCES `transactions` (`srid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `entity_version`
--

LOCK TABLES `entity_version` WRITE;
INSERT INTO `entity_version` VALUES (0,NULL,'17a614da482783fc501c21a98a83d8b44d8d3f9b',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(1,NULL,'84f03d370353770d71e7dc07f9f00c263f026e22',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(2,NULL,'2d2faa9bdbc4cd48f3fcaac54c52668b7c5f1f1c',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(3,NULL,'fa65b9b783c36ff694d9212df96d05e637fd9ecb',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(4,NULL,'07de7952f1651ba3b69117d285ccf28c5ca7d956',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(7,NULL,'2b0276af86c02ac33ceebaaea2a71497a72b2ec1',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(8,NULL,'a5b6b050ccc5ac04fd7078c8dfabad9fd5b59b6b',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(11,NULL,'da8e07ee8867b385db7e553bc8da0d8875deeef1',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(12,NULL,'f572e439aac441e4e58773316dbba38ae6404907',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(13,NULL,'2f9537d5434141dc620487ea7ecef8ef8c6915dd',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(14,NULL,'9658a375370fa8a14026723ed25ea8bffebf4f6b',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(15,NULL,'04bff2ab282998c327e8f5f460db277e5a3d58fb',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(16,NULL,'d3627fe225cfa2381f690de434728d0f546cc8bf',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(17,NULL,'7aa80399376a1912fdc2a996056cab71c1d8c42d',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(18,NULL,'2ef88447ea48cfb21f11cbb2583f86d3d2cda088',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(20,NULL,'75462fb8fe9b2c696cbb9c5e82038c32e6b814c4',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(21,NULL,'5432f3086adbded25101e36d11e3d1948611c49c',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(24,NULL,'29a85dbc0ca204bdb436261cbb429415e2485344',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(50,NULL,'bd511fbff4e2429ded9d022bcd430f0fdad20c5f',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e'),(99,NULL,'be28e81a7391f96a343f6e6c082d73da5b8ce66b',1,NULL,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e');
UNLOCK TABLES;

--
-- Table structure for table `archive_collection_type`
--

DROP TABLE IF EXISTS `archive_collection_type`;
CREATE TABLE `archive_collection_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `collection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_collection_type-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_collection_type_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_collection_type_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_collection_type_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_data_type`
--

DROP TABLE IF EXISTS `archive_data_type`;
CREATE TABLE `archive_data_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `datatype` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_data_type-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `datatype` (`datatype`),
  CONSTRAINT `archive_data_type_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_data_type_ibfk_4` FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_date_data`
--

DROP TABLE IF EXISTS `archive_date_data`;
CREATE TABLE `archive_date_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` int(11) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_date_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_date_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_date_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_datetime_data`
--

DROP TABLE IF EXISTS `archive_datetime_data`;
CREATE TABLE `archive_datetime_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` bigint(20) NOT NULL,
  `value_ns` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_datetime_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_datetime_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_datetime_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_desc_overrides`
--

DROP TABLE IF EXISTS `archive_desc_overrides`;
CREATE TABLE `archive_desc_overrides` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_desc_overrides-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_desc_overrides_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_desc_overrides_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_desc_overrides_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_double_data`
--

DROP TABLE IF EXISTS `archive_double_data`;
CREATE TABLE `archive_double_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` double NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_double_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_double_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_double_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_entities`
--

DROP TABLE IF EXISTS `archive_entities`;
CREATE TABLE `archive_entities` (
  `id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` enum('RECORDTYPE','RECORD','FILE','DOMAIN','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') COLLATE utf8_unicode_ci NOT NULL,
  `acl` int(10) unsigned DEFAULT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`_iversion`),
  KEY `acl` (`acl`),
  CONSTRAINT `archive_entities_ibfk_1` FOREIGN KEY (`id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE,
  CONSTRAINT `archive_entities_ibfk_2` FOREIGN KEY (`acl`) REFERENCES `entity_acl` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_enum_data`
--

DROP TABLE IF EXISTS `archive_enum_data`;
CREATE TABLE `archive_enum_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varbinary(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_enum_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_enum_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_enum_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_files`
--

DROP TABLE IF EXISTS `archive_files`;
CREATE TABLE `archive_files` (
  `file_id` int(10) unsigned NOT NULL,
  `path` text COLLATE utf8_unicode_ci NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `hash` binary(64) DEFAULT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`file_id`,`_iversion`),
  CONSTRAINT `archive_files_ibfk_1` FOREIGN KEY (`file_id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_integer_data`
--

DROP TABLE IF EXISTS `archive_integer_data`;
CREATE TABLE `archive_integer_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` bigint(20) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_integer_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_integer_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_integer_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_isa`
--

DROP TABLE IF EXISTS `archive_isa`;
CREATE TABLE `archive_isa` (
  `child` int(10) unsigned NOT NULL,
  `child_iversion` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `direct` tinyint(1) DEFAULT 1,
  KEY `parent` (`parent`),
  KEY `child` (`child`,`child_iversion`),
  CONSTRAINT `archive_isa_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_isa_ibfk_2` FOREIGN KEY (`child`, `child_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_name_data`
--

DROP TABLE IF EXISTS `archive_name_data`;
CREATE TABLE `archive_name_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `value` (`value`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_name_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_name_overrides`
--

DROP TABLE IF EXISTS `archive_name_overrides`;
CREATE TABLE `archive_name_overrides` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  UNIQUE KEY `archive_name_overrides-d-e-p-v` (`domain_id`,`entity_id`,`property_id`,`_iversion`),
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_name_overrides_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_overrides_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_name_overrides_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_null_data`
--

DROP TABLE IF EXISTS `archive_null_data`;
CREATE TABLE `archive_null_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_null_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_null_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_null_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_query_template_def`
--

DROP TABLE IF EXISTS `archive_query_template_def`;
CREATE TABLE `archive_query_template_def` (
  `id` int(10) unsigned NOT NULL,
  `definition` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`,`_iversion`),
  CONSTRAINT `archive_query_template_def_ibfk_1` FOREIGN KEY (`id`, `_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_reference_data`
--

DROP TABLE IF EXISTS `archive_reference_data`;
CREATE TABLE `archive_reference_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` int(10) unsigned NOT NULL,
  `value_iversion` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `value` (`value`),
  CONSTRAINT `archive_reference_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_reference_data_ibfk_4` FOREIGN KEY (`value`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `archive_text_data`
--

DROP TABLE IF EXISTS `archive_text_data`;
CREATE TABLE `archive_text_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL,
  `_iversion` int(10) unsigned NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`,`_iversion`),
  KEY `domain_id_2` (`domain_id`,`_iversion`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `archive_text_data_ibfk_1` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_text_data_ibfk_2` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE,
  CONSTRAINT `archive_text_data_ibfk_3` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `collection_type`
--

DROP TABLE IF EXISTS `collection_type`;
CREATE TABLE `collection_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `collection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `collection_type-d-e-p` (`domain_id`,`entity_id`,`property_id`),
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  CONSTRAINT `collection_type_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `collection_type_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `collection_type_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `data_type`
--

DROP TABLE IF EXISTS `data_type`;
CREATE TABLE `data_type` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `datatype` int(10) unsigned NOT NULL,
  UNIQUE KEY `datatype_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `name_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `datatype_forkey_ent` (`entity_id`),
  KEY `datatype_forkey_pro` (`property_id`),
  KEY `datatype_forkey_type` (`datatype`),
  CONSTRAINT `datatype_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `datatype_forkey_type` FOREIGN KEY (`datatype`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- TODO Data for table `data_type`
--

LOCK TABLES `data_type` WRITE;
INSERT INTO `data_type` VALUES (0,0,20,14),(0,0,21,14),(0,0,24,14);
UNLOCK TABLES;

--
-- Table structure for table `date_data`
--

DROP TABLE IF EXISTS `date_data`;
CREATE TABLE `date_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `value` int(11) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `date_data_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `date_ov_forkey_ent` (`entity_id`),
  KEY `date_ov_forkey_pro` (`property_id`),
  CONSTRAINT `date_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `date_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `date_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `datetime_data`
--

DROP TABLE IF EXISTS `datetime_data`;
CREATE TABLE `datetime_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `value_ns` int(10) unsigned DEFAULT NULL,
  `value` bigint(20) NOT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `dat_entity_id_entity` (`entity_id`),
  KEY `dat_property_id_entity` (`property_id`),
  CONSTRAINT `dat_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dat_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dat_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `desc_overrides`
--

DROP TABLE IF EXISTS `desc_overrides`;
CREATE TABLE `desc_overrides` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `desc_ov_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `desc_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `desc_ov_forkey_ent` (`entity_id`),
  KEY `desc_ov_forkey_pro` (`property_id`),
  CONSTRAINT `desc_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `desc_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `desc_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `double_data`
--

DROP TABLE IF EXISTS `double_data`;
CREATE TABLE `double_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` double NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `dou_entity_id_entity` (`entity_id`),
  KEY `dou_property_id_entity` (`property_id`),
  CONSTRAINT `dou_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dou_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `dou_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `enum_data`
--

DROP TABLE IF EXISTS `enum_data`;
CREATE TABLE `enum_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `value` varbinary(255) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `enum_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `enum_ov_forkey_ent` (`entity_id`),
  KEY `enum_ov_forkey_pro` (`property_id`),
  CONSTRAINT `enum_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `enum_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `enum_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `feature_config`
--

DROP TABLE IF EXISTS `feature_config`;
CREATE TABLE `feature_config` (
  `_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `feature_config`
--

LOCK TABLES `feature_config` WRITE;
INSERT INTO `feature_config` VALUES ('ENTITY_VERSIONING','ENABLED');
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `file_id` int(10) unsigned NOT NULL COMMENT 'The file''s ID.',
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Directory of the file.',
  `size` bigint(20) unsigned NOT NULL COMMENT 'Size in kB (oktet bytes).',
  `hash` binary(64) DEFAULT NULL,
  `checked_timestamp` bigint(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`file_id`),
  CONSTRAINT `fil_file_id_entity` FOREIGN KEY (`file_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `integer_data`
--

DROP TABLE IF EXISTS `integer_data`;
CREATE TABLE `integer_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` bigint(20) NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `unit_sig` bigint(20) DEFAULT NULL,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `int_entity_id_entity` (`entity_id`),
  KEY `int_property_id_entity` (`property_id`),
  CONSTRAINT `int_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `int_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `int_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `isa_cache`
--

DROP TABLE IF EXISTS `isa_cache`;
CREATE TABLE `isa_cache` (
  `child` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `rpath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`child`,`parent`,`rpath`),
  KEY `isa_cache_parent_entity` (`parent`),
  CONSTRAINT `isa_cache_child_entity` FOREIGN KEY (`child`) REFERENCES `entities` (`id`),
  CONSTRAINT `isa_cache_parent_entity` FOREIGN KEY (`parent`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- TODO Table structure for table `logging`
--

-- DROP TABLE IF EXISTS `logging`;
-- CREATE TABLE `logging` (
--   `level` int(11) NOT NULL,
--   `logger` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
--   `message` mediumtext COLLATE utf8_unicode_ci NOT NULL,
--   `millis` bigint(20) NOT NULL,
--   `logRecord` blob NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `name_data`
--

DROP TABLE IF EXISTS `name_data`;
CREATE TABLE `name_data` (
  `domain_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  UNIQUE KEY `domain_id_2` (`domain_id`,`entity_id`,`property_id`),
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `entity_id` (`entity_id`),
  KEY `property_id` (`property_id`),
  KEY `value` (`value`),
  CONSTRAINT `name_data_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_data_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_data_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `name_data`
--

LOCK TABLES `name_data` WRITE;
INSERT INTO `name_data` VALUES (0,0,20,'DOMAIN','FIX',0),(0,1,20,'RECORDTYPE','FIX',0),(0,2,20,'RECORD','FIX',0),(0,3,20,'FILE','FIX',0),(0,4,20,'PROPERTY','FIX',0),(0,7,20,'DATATYPE','FIX',0),(0,8,20,'QUERYTEMPLATE','FIX',0),(0,11,20,'REFERENCE','FIX',0),(0,12,20,'INTEGER','FIX',0),(0,13,20,'DOUBLE','FIX',0),(0,14,20,'TEXT','FIX',0),(0,15,20,'DATETIME','FIX',0),(0,16,20,'TIMESPAN','FIX',0),(0,17,20,'FILE','FIX',0),(0,18,20,'BOOLEAN','FIX',0),(0,20,20,'name','FIX',0),(0,21,20,'unit','FIX',0),(0,24,20,'description','FIX',0),(0,50,20,'SQLITE','FIX',0);
UNLOCK TABLES;

--
-- Table structure for table `name_overrides`
--

DROP TABLE IF EXISTS `name_overrides`;
CREATE TABLE `name_overrides` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `name_ov_ukey` (`domain_id`,`entity_id`,`property_id`),
  KEY `name_ov_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `name_ov_forkey_ent` (`entity_id`),
  KEY `name_ov_forkey_pro` (`property_id`),
  CONSTRAINT `name_ov_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_ov_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `name_ov_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `null_data`
--

DROP TABLE IF EXISTS `null_data`;
CREATE TABLE `null_data` (
  `domain_id` int(10) unsigned DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX') COLLATE utf8_unicode_ci DEFAULT NULL,
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `null_data_dom_ent_idx` (`domain_id`,`entity_id`),
  KEY `null_forkey_ent` (`entity_id`),
  KEY `null_forkey_pro` (`property_id`),
  CONSTRAINT `null_forkey_dom` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `null_forkey_ent` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `null_forkey_pro` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `passwd`
--

DROP TABLE IF EXISTS `passwd`;
CREATE TABLE `passwd` (
  `principal` varbinary(255) NOT NULL,
  `hash` varbinary(255) NOT NULL,
  `alg` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SHA-512',
  `it` int(10) unsigned DEFAULT 5000,
  `salt` varbinary(255) NOT NULL,
  PRIMARY KEY (`principal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `name` varbinary(255) NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
INSERT INTO `roles` VALUES ('administration','Users with this role have unrestricted permissions.'),('anonymous','Users who did not authenticate themselves.');
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `role` varbinary(255) NOT NULL,
  `permissions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`role`),
  CONSTRAINT `perm_name_roles` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
INSERT INTO `permissions` VALUES ('administration','[{\"grant\":\"true\",\"priority\":\"true\",\"permission\":\"*\"}]');
UNLOCK TABLES;

--
-- Table structure for table `query_template_def`
--

DROP TABLE IF EXISTS `query_template_def`;
CREATE TABLE `query_template_def` (
  `id` int(10) unsigned NOT NULL,
  `definition` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `query_template_def_ibfk_1` FOREIGN KEY (`id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `reference_data`
--

DROP TABLE IF EXISTS `reference_data`;
CREATE TABLE `reference_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` int(10) unsigned NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  `value_iversion` int(10) unsigned DEFAULT NULL,
  KEY `entity_id` (`entity_id`,`property_id`),
  KEY `ref_domain_id_entity` (`domain_id`),
  KEY `ref_property_id_entity` (`property_id`),
  KEY `value` (`value`,`value_iversion`),
  CONSTRAINT `ref_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `ref_value_entity` FOREIGN KEY (`value`) REFERENCES `entities` (`id`),
  CONSTRAINT `reference_data_ibfk_1` FOREIGN KEY (`value`, `value_iversion`) REFERENCES `entity_version` (`entity_id`, `_iversion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
CREATE TABLE `stats` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` blob DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `text_data`
--

DROP TABLE IF EXISTS `text_data`;
CREATE TABLE `text_data` (
  `domain_id` int(10) unsigned NOT NULL COMMENT 'Domain.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity.',
  `property_id` int(10) unsigned NOT NULL COMMENT 'Property.',
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('OBLIGATORY','RECOMMENDED','SUGGESTED','FIX','REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Status of this statement.',
  `pidx` int(10) unsigned NOT NULL DEFAULT 0,
  KEY `domain_id` (`domain_id`,`entity_id`),
  KEY `str_entity_id_entity` (`entity_id`),
  KEY `str_property_id_entity` (`property_id`),
  CONSTRAINT `str_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `str_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
  CONSTRAINT `str_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `transaction_log`
--

DROP TABLE IF EXISTS `transaction_log`;
CREATE TABLE `transaction_log` (
  `transaction` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Transaction.',
  `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity ID.',
  `username` varbinary(255) NOT NULL,
  `seconds` bigint(20) unsigned NOT NULL DEFAULT 0,
  `nanos` int(10) unsigned NOT NULL DEFAULT 0,
  `realm` varbinary(255) NOT NULL,
  KEY `entity_id` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `units_lin_con`
--

DROP TABLE IF EXISTS `units_lin_con`;
CREATE TABLE `units_lin_con` (
  `signature_from` bigint(20) NOT NULL,
  `signature_to` bigint(20) NOT NULL,
  `a` decimal(65,30) NOT NULL,
  `b_dividend` int(11) NOT NULL,
  `b_divisor` int(11) NOT NULL,
  `c` decimal(65,30) NOT NULL,
  PRIMARY KEY (`signature_from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `realm` varbinary(255) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `email` varbinary(255) DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `entity` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`realm`,`name`),
  KEY `subject_entity` (`entity`),
  CONSTRAINT `subjects_ibfk_1` FOREIGN KEY (`entity`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `realm` varbinary(255) NOT NULL,
  `user` varbinary(255) NOT NULL,
  `role` varbinary(255) NOT NULL,
  PRIMARY KEY (`realm`,`user`,`role`),
  KEY `user_roles_ibfk_1` (`role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

