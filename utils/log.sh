#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

#start and stop logging; get current logs

if [ -z "$UTILSPATH" ]; then
	UTILSPATH="$(realpath $(dirname $0))"
	export UTILSPATH
	MAINPATH="$(dirname $UTILSPATH)"
	export MAINPATH
fi

# Load settings from .config and defaults #####################################
. $UTILSPATH/load_settings.sh

# load useful functions #######################################################
. $UTILSPATH/helpers.sh

function get_logs {
    echo 'select event_time, argument from mysql.general_log where command_type="Query";' | \
		$MYSQL_CMD $(get_db_args_nodb) --batch
}

function set_logging {
    echo "Setting logging $1..."
    echo "SET GLOBAL log_output = 'TABLE'; SET GLOBAL general_log = '$1';" | \
        $MYSQL_CMD $(get_db_args_nodb) --batch
	success
}

case $1 in
    "start") set_logging "on" ;;
    "stop") set_logging "off" ;;
    "get") get_logs ;;
    *) echo "Unknown argument: $1"; exit 1
esac
