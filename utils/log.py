#!/usr/bin/env python3

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Daniel Hornung, IndiScale GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Remote module for SQL logging

Basically a wrapper with nice arguments around the bash script log.sh
"""

import argparse
import os
import subprocess
import sys

UTILS_PATH = os.path.abspath(os.path.dirname(__file__))
COMMAND = [os.path.join(UTILS_PATH, "log.sh")]


def _store_log():
    """Retrieves the log table and returns it as a TSV string.

    Returns
    -------
    out : str
      The string representing the logging information, as TSV.
    """
    result = subprocess.run(COMMAND+["get"], encoding="utf8",
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # capture_output=True)  ## Only works since 3.7

    if result.returncode > 0:
        print("Error during SQL command, while retrieving SQL logs:", file=sys.stderr)
    print(result.stderr, file=sys.stderr)
    print(result.stdout)


def _logging(turn_on):
    """Turn SQL logging off/on.

This function tells the SQL database to turn logging off or on (depending on the
arguments).  It returns nothing.

Parameters
----------
turn_on : bool
  Whether to turn logging on or off.
    """
    result = subprocess.run(COMMAND+["start" if turn_on else "stop"],
                            encoding="utf8",
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # capture_output=True)  ## Only works since 3.7

    if result.returncode > 0:
        print("Error during SQL command, while setting SQL logging to '{on_off}':".format(
            on_off=on_off), file=sys.stderr)
    print(result.stderr, file=sys.stderr)
    print(result.stdout)

    # Return nothing if no error.


def start_logging(args):
    _logging(True)


def stop_logging(args):
    _logging(False)


def store(args):
    _store_log()


def parse_args():
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__+"""
To view additional information about subcommands, execute:
    linkahead <subcmd> -h""",
        prog="mysqllog",
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    subparsers = parser.add_subparsers(
        help='The action to take.', dest="action", required=True)

    # mysql logging
    start_parser = subparsers.add_parser(
        'on',
        help='Turns SQL logging on.')
    start_parser.set_defaults(func=start_logging)

    stop_parser = subparsers.add_parser(
        'off',
        help='Turns SQL logging off.')
    stop_parser.set_defaults(func=stop_logging)

    stop_parser = subparsers.add_parser(
        'store',
        help='Prints SQL logs.')
    stop_parser.set_defaults(func=store)

    return parser.parse_args()


if __name__ == "__main__":
    """The main function."""
    args = parse_args()
    args.func(args)
