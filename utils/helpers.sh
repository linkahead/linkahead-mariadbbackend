# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2019, 2020, 2024 Daniel Hornung (d.hornung@indiscale.com)
# Copyright (C) 2020 Henrik tom Wörden
# Copyright (C) 2020, 2024 IndiScale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.



###############################################################################
#                         Helper function definitions                         #
###############################################################################

# When sourcing this file, the following variables must be set:
#
# MYSQL_CMD : source $UTILSPATH/load_settings.sh



function mysql_execute {
	  #TODO is it meaningful that here always the database user is used???
	  set -e
	  $MYSQL_CMD $(get_db_args) -e "$1"
	  ret=${PIPESTATUS[0]}
	  if [ "$ret" -ne 0 ]; then
		    failure "MYSQL ERROR"
	  fi
}


# Takes a filename with commands instead of command line arguments
function mysql_execute_file {
	set -e
	$MYSQL_CMD $(get_db_args) < "$1"
	ret=${PIPESTATUS[0]}
	if [ "$ret" -ne 0 ]; then
		failure "MYSQL ERROR"
	fi
}

# Return the arguments from `get_db_args_nodb` and additionally the database name as
# `--database=mydatabasename`.
function get_db_args {
	echo "$(get_db_args_nodb) --database=$DATABASE_NAME"
}

# Return arguments for user, password, host, port and additional options in MYSQL_OPTS.
#
# For example, the output may be:
# --user=myuser --password=mypassword --host=example.com --port=1234 --other --option
function get_db_args_nodb {
	if [ -n "$MYSQL_USER" ]; then
		mysql_con_arguments="--user=$MYSQL_USER"
	fi
	if [ -n "$MYSQL_USER_PASSWORD" ]; then
		mysql_con_arguments="$mysql_con_arguments --password=$MYSQL_USER_PASSWORD"
	fi
	if [ -n "$MYSQL_HOST" ] && [ "$MYSQL_HOST" != "localhost" ]; then
		mysql_con_arguments="$mysql_con_arguments --host=$MYSQL_HOST"
	fi
	if [ -n "$MYSQL_PORT" ] && [ "$MYSQL_PORT" != "3306" ]; then
		mysql_con_arguments="$mysql_con_arguments --port=$MYSQL_PORT"
	fi
	if [ -n "$MYSQL_OPTS" ] ; then
		mysql_con_arguments="$mysql_con_arguments $MYSQL_OPTS"
	fi
	echo $mysql_con_arguments
}

function success {
    echo "[OK]"
    exit 0
}

function failure {
    echo "[FAILED] $*"
    exit 1
}

function uptodate {
    echo "[UPTODATE]"
    exit 0
}
