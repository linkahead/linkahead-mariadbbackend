#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2019, 2020 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2020 IndiScale <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

###############################################################################
#                          header for patch scripts                           #
###############################################################################

set -e

# load useful functions #######################################################
source $UTILSPATH/load_settings.sh
source $UTILSPATH/helpers.sh

USAGE="$1 [ --env=ENV_FILE ] [ --patch=PATCH ] [ --backupdir=BACKUPDIR ]\n\n"
CMD_OPTIONS=$(cat <<EOF
options:

-h, --help
    Show brief help.
--env=ENV_FILE
    A file where variables are stored in the format of shell environment
    variables.  Content of this file overrides environment variables already
    present.
--patch=PATCH
    *TODO* Please document this option.
--backupdir=BACKUPDIR
    The directory where backup files are stored to.
--yaml[=True|False]
    If given (and the optional value is not False), additional machine-readable
    YAML output is emitted.  Only implemented for some commands yet.

This script basically uses the same environment variables as the server
configuration make file.  Notable examples are:

- LOGIN_PATH
- MYSQL_HOST
- MYSQL_PORT
- DATABASE_NAME
- DATABASE_USER
- DATABASE_USER_PW

EOF
              )

function _print_help() {
  echo -e "$USAGE"
  echo -e "$CMD_OPTIONS"
  if [ -n "$PRINT_HELP" ]; then
    echo -e "$PRINT_HELP"
  fi
  if [ -n "$1" ]; then
    echo -e "$1"
  fi
}

while test $# -gt 0; do
 case "$1" in
 -h|--help)
  _print_help
  exit 0
  ;;
 --env*)
  ENV_FILE="${1#--*=}"
  shift
  ;;
 --patch*)
  PATCH="${1#--*=}"
  PATCH_DIR=$(dirname $PATCH)
  shift
  ;;
 --backupdir*)
  BACKUPDIR="${1#--*=}"
  shift
  ;;
 --yaml*)
     if [[ "$1" == "--yaml" ]] ; then
         YAML="True"
     else
         YAML="${1#--*=}"
     fi
  shift
  ;;
 *)
  echo "Unknown option $1"
  exit 1
  shift
  ;;
 esac
done


if [ -n "$PATCH" ]; then
    echo -ne "applying patch $PATCH to $DATABASE_NAME ... "
fi
# @param $1: db version string, e.g. v2.0.0
# @return: 0 on success, 1 on failure
function check_version {
    local version=$($MYSQL_CMD $(get_db_args) -B -e "Select CaosDBVersion();")
	  if [[ "$(echo $version | sed 's/^CaosDBVersion()\s//')" = "$1" ]]; then
        return 0
	  fi
    uptodate
}

# @param $1: new version string
function update_version {
    mysql_execute "DROP FUNCTION IF EXISTS CaosDBVersion; CREATE FUNCTION CaosDBVersion() RETURNS VARCHAR(255) DETERMINISTIC RETURN '$1';"
}

function dump_table {
    if [[ -z $MYSQLDUMP_CMD ]]; then
        echo "Cannot find mysqldump program!" >&2
        exit 2
    fi
    $MYSQLDUMP_CMD $(get_db_args_nodb) $DATABASE_NAME $1 \
                    > ${DATABASE_NAME}.${1}.${OLD_VERSION}.dump.sql
}

function redo_table {
         $MYSQL_CMD $(get_db_args) < ${DATABASE_NAME}.${1}.${OLD_VERSION}.dump.sql
}


