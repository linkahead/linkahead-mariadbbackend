

DROP PROCEDURE IF EXISTS sanity_check;
delimiter //
CREATE PROCEDURE sanity_check()
sanityCheckBody: BEGIN
    CREATE TEMPORARY TABLE expected_tables (table_name VARCHAR(255));
    INSERT INTO expected_tables (table_name) VALUES
("archive_collection_type"),
("archive_data_type"),
("archive_date_data"),
("archive_datetime_data"),
("archive_desc_overrides"),
("archive_double_data"),
("archive_entities"),
("archive_enum_data"),
("archive_files"),
("archive_integer_data"),
("archive_isa"),
("archive_name_data"),
("archive_name_overrides"),
("archive_null_data"),
("archive_query_template_def"),
("archive_reference_data"),
("archive_text_data"),
("collection_type"),
("data_type"),
("date_data"),
("datetime_data"),
("desc_overrides"),
("double_data"),
("entities"),
("entity_ids"),
("entity_acl"),
("entity_version"),
("enum_data"),
("feature_config"),
("files"),
("integer_data"),
("isa_cache"),
("name_data"),
("name_overrides"),
("null_data"),
("passwd"),
("permissions"),
("query_template_def"),
("reference_data"),
("roles"),
("stats"),
("text_data"),
("transaction_log"),
("transactions"),
("units_lin_con"),
("user_info"),
("user_roles")
;

SELECT COUNT(WRONG) INTO @count_wrong FROM ( SELECT l.table_name AS MATCHED, r.table_name AS WRONG FROM expected_tables AS l RIGHT OUTER JOIN information_schema.tables AS r ON (r.table_name COLLATE utf8_unicode_ci = l.table_name) WHERE r.table_schema = database()) AS temp WHERE temp.MATCHED IS NULL;

SELECT COUNT(MISSING) INTO @count_missing FROM ( SELECT l.table_name AS MISSING, r.table_name AS MATCHED FROM expected_tables AS l LEFT OUTER JOIN information_schema.tables AS r ON (r.table_name COLLATE utf8_unicode_ci = l.table_name) WHERE r.table_schema = database() OR r.table_schema IS NULL) AS temp WHERE temp.MATCHED IS NULL;

IF @count_missing = 0 AND @count_wrong = 0 THEN
    LEAVE sanityCheckBody;
END IF;

SELECT "--------------";
SELECT @count_missing AS "Number of missing tables";

SELECT MISSING AS "Missing tables" FROM ( SELECT l.table_name AS MISSING, r.table_name AS MATCHED FROM expected_tables AS l LEFT OUTER JOIN information_schema.tables AS r ON (r.table_name COLLATE utf8_unicode_ci = l.table_name) WHERE r.table_schema = database() OR r.table_schema IS NULL) AS temp WHERE temp.MATCHED IS NULL;

SELECT "--------------";
SELECT @count_wrong AS "Number of tables which should not exist";

SELECT WRONG AS "Tables which should not exist" FROM ( SELECT l.table_name AS MATCHED, r.table_name AS WRONG FROM expected_tables AS l RIGHT OUTER JOIN information_schema.tables AS r ON (r.table_name COLLATE utf8_unicode_ci = l.table_name) WHERE r.table_schema = database()) AS temp WHERE temp.MATCHED IS NULL;

SELECT "--------------";
SELECT "ERROR" from sanity_check_failed;

END;
//
delimiter ;

CALL sanity_check();

DROP PROCEDURE sanity_check;
