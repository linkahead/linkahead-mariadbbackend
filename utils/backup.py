#!/usr/bin/env python3
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 Daniel Hornung (d.hornung@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""A thin wrapper around the sql backup bash script.

It provides a meaningful default directory and a nice help message
"""

import argparse
import os
import subprocess
import tempfile


def create_dump(directory=None):
    """Create the SQL dump

Parameters :
------------------------------
dictionary : str
    If not None, the place to write the SQL dump into.
    """

    utils_path = os.path.dirname(__file__)
    command = [os.path.join(utils_path, "backup.sh")]
    env = os.environ.copy()

    if not directory:
        os.makedirs("/tmp/caosdb/tmpfiles/backup/", exist_ok=True)
        directory = tempfile.mkdtemp(dir="/tmp/caosdb/tmpfiles/backup/",
                                     prefix="backup.")
    env["BACKUPDIR"] = directory
    subprocess.run(command, cwd=os.path.dirname(__file__), env=env)


def parse_args():
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        prog="mysql backup",
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        '-d', "--directory",
        help='Directory to which the sql dump will be saved.')

    return parser.parse_args()


if __name__ == "__main__":
    """The main function."""
    args = parse_args()
    create_dump(args.directory)
