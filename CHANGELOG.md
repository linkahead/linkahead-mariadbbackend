# Changelog #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [8.0.0] - 2024-10-24 ##

### Added ###

- Script to update outdated database dumps.

### Changed ###

- `OFFSET` is a reserved keyword in MariaDB 10.6, so the former
  `OFFSET` parameter name in the `get_head_relative` function was
  changed to HeadOffset. See
  [`update_dumps/README.md`](update_dumps/README.md) for information
  on how to update old SQL dumps to be compatible with this renaming
  and MariaDB >= 10.6.

## [7.0.2] - 2023-12-12 ##
(Timm Fitschen)

### Fixed ###

* make install: Could not connect to MariaDB in local docker
  container
  [linkahead-mariadbbackend](https://gitlab.com/linkahead/linkahead-mariadbbackend/-/issues/33)
* Wrong ids in data_type table due to a bug in the `linkahead-server < v0.11.0`
  which has been fixed but left broken data. This resulted in server errors
  [linkahead-mariadbbackend#34](https://gitlab.com/linkahead/linkahead-mariadbbackend/-/issues/34)

## [7.0.1] - 2023-11-01
(Timm Fitschen)

### Added ###

* Sanity checks during `make install` and `make upgrade`.

## [7.0.0] - 2023-10-25 ##
(Timm Fitschen)

### Changed ###

* Change signature of procedure `insertEntityDataType`: expect the data type ID
  instead of the name.
* Change result set of procedure `retrieveEntity`: Rename column `Datatype` to
  `DatatypeName` and add column  `DatatypeID`.
* Change result set of procedure `retrieveOverrides`: Rename column
  `type_override` to `type_name_override` and add `type_id_override`.
* Change signature of procedure `updateEntity`: expect the data type ID instead
  to the name.
* Internal ids, when returned by `retrieveEntityProperties`,
  `registerReplacementIds`, or `retrieveOverrides`, are prefixed by a special
  character which serves as a ephemeral marker. Currently this is `$` but this
  may change in the future if there is a more suitable candidate.

### Deprecated ###

* MySQL Support. Last version which is known to work well with LinkAhead is MySQL 5.7.36

### Fixed ###

* Unknown Server Error when inserting an Entity.
  [linkahead-mariadbbackend#48](https://gitlab.indiscale.com/caosdb/src/caosdb-mysqlbackend/-/issues/48).
* Fix broken `initEntity` procedure (and change the first parameters type from
  INT UNSIGNED to VARCHAR(255) which is non-breaking).

## [6.0.1 2023-10-18] ##

### Fixed ###
* Old passwords table is removed from dumps where it still exists.

## [6.0.0 2023-10-17] ##

This is a major update. Switching from integer ids for internal and external use to string ids for the external use while keeping the old integer ids for internal use.

### Added ###

* Table `entity_ids`
* Procedure `getIdByName`
* Procedure `insertEntityDataType` and `insertEntityCollection`
* Procedure `setFileProperties`.

### Changed ###

* Rename PROCEDURE `registerSubdomain` to `registerReplacementIds`.
* Change column `entities.role` datatype: replace 'DOMAIN' enum value with `_REPLACEMENT`.
* Change column `transactions_log.entity_id` to VARCHAR(255)
* Change column `user_info.entity` to VARCHAR(255)
* Change signature of procedure `applyIDFilter` - `EntityID VARCHAR(255)`
* Change signature of procedure `initBackreference` - `EntityID VARCHAR(255)` and `PropertyID VARCHAR(255)`
* Change signature of procedure `initPOVPropertiesTable` - `PropertyID VARCHAR(255)`
* Change signature of procedure `initPOVRefidsTable` - `PropertyID VARCHAR(255)`
* Change signature of procedure `initSubEntity` - `EntityID VARCHAR(255)`
* Change signature of procedure `deleteEntity` - `EntityID VARCHAR(255)`
* Change signature of procedure `deleteEntityProperties` - `EntityID VARCHAR(255)`
* Change signature of procedure `get_primary_parent_version` - `EntityID VARCHAR(255)`
* Change signature of procedure `get_version_timestamp` - `EntityID VARCHAR(255)`
* Change signature of procedure `get_head_version` - `EntityID VARCHAR(255)`
* Change signature of procedure `get_head_relative` - `EntityID VARCHAR(255)`
* Change signature of procedure `get_version_history` - `EntityID VARCHAR(255)`
* Change signature of procedure `retrieveQueryTemplateDef` - `EntityID VARCHAR(255)`
* Change signature of procedure `getDependentEntities` - `EntityID VARCHAR(255)`
* Change signature of procedure `insertEntity` - Add parameter `EntityID VARCHAR(255)`
* Change signature of procedure `insertEntityProperty` - `EntityID VARCHAR(255)` and `PropertyID VARCHAR(255)`, `DomainID VARCHAR(255)`, `DatatypeOverride VARCHAR(255)`
* Change signature of procedure `insertIsa` - `ChildID VARCHAR(255)`, `ParentID VARCHAR(255)`.
* Change signature of procedure `isSubtype` - `ChildID VARCHAR(255)`, `ParentID VARCHAR(255)`.
* Change signature of procedure `retrieveEntity` - `EntityID VARCHAR(255)`
* Change signature of procedure `retrieveOverrides` - `EntityID VARCHAR(255)`, `DomainID VARCHAR(255)`
* Change signature of procedure `retrieveEntityParents` - `EntityID VARCHAR(255)`
* Change signature of procedure `retrieveEntityProperties` - `EntityID VARCHAR(255)`, `DomainID VARCHAR(255)`
* Change signature of procedure `updateEntity` - `EntityID VARCHAR(255)`

### Removed ###

* Deactivate procedure `delete_all_entity_versions`. This might be used in the
  future, that is why we keep the code in a comment.
* Drop procedure `retrieveSubEntity`
* Drop procedure `retrieveDatatype`
* Drop procedure `retrieveGroup`
* Drop procedure `getInfo`
* Drop procedure `getRole`
* Drop procedure `setPassword`
* Drop procedure `initAutoIncrement`
* Delete special entity 50 (SQLite Datatype)
* Delete special entity 99 (Work-around for the auto-increment)

### Fixed ###

* Change signature of procedure `getFileIdByPath` - `FilePath TEXT` - this length is needed for the path column of `file_entities`.

## [5.0.0] - 2021-10-28 ##

### Added ###

* #33 CI pipeline for MySQL (was only MariaDB before).

### Removed ###

* `getRules` procedure and the `rules` table. The jobs rules are being
  configured in the server repository now.

### Fixed ###

* Bug in applyPOV which led to caosdb-server#154: Queries with "!=" returned
  the same results as "=" (for references).
* #32 Removed unused `groups` table from installation routines which prevented
  the installation with MySQL 8.

## [4.1.0] - 2021-06-11 ##

### Added ###

- `utils/make_db` has new `grant-permission` command.

## [4.0.0] - 2021-02-10 ##

### Added ###

* Added a `_get_head_iversion` for non-api usage.
* Automated documentation builds: `make doc`

### Changed ###

* `retrieveEntity` does not return the columns `VersionSeconds` and
  `VersionNanos` anymore.
* `get_version_history` returns two additional columns, `child_username` and
  `child_realm`.
* Added a `versioned` flag to the following procedures:
    * `applyBackReference`
    * `applyIDFilter`
    * `applyPOV`
    * `applyRefPOV`
    * `makeStmt`
    * `calcComplementUnion`
    * `calcDifference`
    * `calcIntersection`
    * `finishSubProperty`
    * `getChildren`
    * `initEmptyTargetSet`
    * `initDisjunctionFilter`
    * `initEntity`
    * `initQuery`
    * `createTmpTable`
* Added a `direct` column to `archive_isa` table

### Removed ###

* unused procedures:
    * `initNegationFilter`
    * `initConjunctionFilter`
    * `finishNegationFilter`

### Fixed ###

* #23 Incompatibility with MariaDB 10.3
* Fixed the `>=` and `<=` operators for comparisons in POV query filters with
  stored DATE values. The fix makes the implementation consistent
  However, the current definition and implementation of the
  `>=` and `<=` operators for date and datetime is unintuitive and the operators
  should have another name. Something like "overlap with or smaller/greater
  than".
* Semi-fix in `retrieveEntityParents`. The old implementation was buggy and
  would return no parent name or even a wrong one for old entity versions in
  some cases. The semi-fix will allways return the current name of the parent
  (even if this is historically not always correct). A real fix awaits the
  implementation of versioned isa-relations which is already planned.
* Bug in `retrieveOverrides` function where selecting the datatype of an old
  version resultet in an error. See corresponding test in `caosdb-pyinttest`
  `tests/test_versioning.py::test_datatype_without_name`

## [3.0.0] - 2020-09-01 ##

### Added ###

* New `feature_config` table for storing configuration of features as key-value
  pairs. Currently only used by the ENTITY_VERSIONING feature for switching
  versioning on or off. Convenient function `is_feature_config` for checking
  whether the `feature_config` table has a particular value.
* New `transactions` table. This is necessary for the ENTITY_VERSIONING feature
  and will replace the `transaction_log` table in the future.
* Feature ENTITY_VERSIONING (experimental)
  Switch off this feature with `DELETE FROM feature_config WHERE
  _key="ENTITY_VERSIONING"` and switch on with `INSERT INTO feature_config
  (_key, _value) VALUES ("ENTITY_VERSIONING", "ENABLED")`. This feature comes
  with a lot of additions to the API. E.g.
  * New `entity_version`.
  * All `*_data` tables have a new twin, the `archive_*_data` table, where all
    old versions of entities are stored. The `*_data` tables only contain the
    data of the latest version.
  * Additional `archive_isa` for the history entities' parents.
  * Additional `_iversion` column for the `reference_data` table for storing
    references to particular versions of an entity.
  * New `setFileProperties` and `retrieveQueryTemplateDef` procedures which reduce server code and let the
    backend decide which tables to use. Also, this is necessary for the
    versioning, because these procedures behave differently depending on the
    ENTITY_VERSIONING feature being enabled or disabled.
  * Several functions and procedures for the interaction with the
    `entity_version` table and the `transactions` table. E.g.
    `insert_single_child_version`, `delete_all_entity_versions`,
    `get_iversion`, `get_primary_parent_version`, `get_version_timestamp`,
    `get_head_version`, `get_head_relative`, `get_version_history`.
  The versions are tracked internally by the `_iversion` field which is an
  integer and which should not be used outside of the backend.
* New makefile targets for testing with MariaDB instance from Docker image: call
  `make test-docker` to temporarily start a Docker container with MariaDB which
  will be used for the unit tests.

### Changed ###

* Removed `getFile` procedure.

### Deprecated ###

* Table `transaction_log` is deprecated. The functionality is being replaced by the `transactions` table.

### Fixed ###

* POV with the 'name' property, e.g. `FIND ENTITY WITH name = something`
  [caosdb-server#51](https://gitlab.com/caosdb/caosdb-server/-/issues/51)
* Fixed several bugs when an Entity inherits from itself (#18, caosdb-server #85).
* Bug in `updateEntity.sql` (when updating the primary name without a prior call
  to `deleteEntityProperties`). Same thing for `deleteEntity`.
* #21 Bug which prevented deletion of deeply inheriting entities, if versioning was enabled.
