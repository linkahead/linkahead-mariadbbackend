/* Just a short snippt with the problem */


/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `get_head_relative`(EntityID VARCHAR(255),
    Offset INT UNSIGNED) RETURNS varbinary(255)
    READS SQL DATA
BEGIN
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;

    
    
    
    
    RETURN (
        SELECT e.version
            FROM entity_version AS e
            WHERE e.entity_id = InternalEntityID
            ORDER BY e._iversion DESC
            LIMIT 1 OFFSET Offset
        );
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
/*!50003 D */
