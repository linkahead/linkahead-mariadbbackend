#!/usr/bin/env python3

# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 IndiScale GmbH <www.indiscale.com>
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Testing the dump update scripts
"""

import filecmp
from pathlib import Path
from subprocess import run
from tempfile import NamedTemporaryFile


def get_basedir() -> str:
    """Return the assumped base dir for the dump updates.
    """
    path = Path(__file__).parents[1]
    return str(path)


def get_test_data(basename: str) -> list[tuple[str, str]]:
    """Return a list of [input, expectedoutput] tuples.

    The output may be an empty string if no corresponding file can be found.
    """
    basedir = get_basedir()
    datadir = Path(basedir) / "test" / "test_data"
    results = []
    for input_path in datadir.glob(f"{basename}.example*[0-9].sql"):
        expected_path = datadir / f"{input_path.name[:-4]}.expected.sql"
        if expected_path.exists():
            expected = str(expected_path)
        else:
            expected = ""
        results.append((str(input_path), expected))
    return results


def test_2024_10_02(tmpdir):
    """``Offset`` became a reserved keyword in MariaDB 10.6.
    """
    script = "2024-10-02.dump_fix_mariadb_10_6.sh"
    script_fullname = str(Path(get_basedir()) / script)
    test_data = get_test_data(script[:-3])
    for infile, expectedfile in test_data:
        with (NamedTemporaryFile(dir=tmpdir, suffix=".sql", delete=True) as output,
              open(infile, mode="rb") as infile_stream
              ):
            run([script_fullname],
                stdin=infile_stream,
                stdout=output,
                check=True
                )
            assert filecmp.cmp(output.name, expectedfile), "Output does not match expected output."
