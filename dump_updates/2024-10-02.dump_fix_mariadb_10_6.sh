#!/usr/bin/env bash

# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# About this script
# =================
#
# Fix the use of "offset" as a procedure parameter name.  "OFFSET" became a reserved keyword in
# MariaDB 10.6.
#
# This script takes an SQL dump from stdin and prints the fixed SQL to stdout.
#
# Usage
# -----
#
# 2024-10-02.dump_fix_mariadb_10_6.sh < yourdump.sql > yourdump.fixed.sql

set -euo pipefail
IFS=$'\n\t'

script='
s/Offset INT UNSIGNED) RETURNS varbinary(255)/HeadOffset INT UNSIGNED) RETURNS varbinary(255)/
s/LIMIT 1 OFFSET Offset/LIMIT 1 OFFSET HeadOffset/
'

sed -e "$script"

unset script
