# SQL dump updates #

## Dump ##

This directory contains scripts to update database dumps to newer versions of the MariaDB server.

## Background ##

In some cases, restoring the database content from existing SQL dumps may not be possible in a
straightforward manner.  For those cases, this directory contains scripts to help with known issues.

Examples for problems include:

- New reserved keywords in MariaDB, that were previously used as identifiers in code.

# Known issues and their fix #

- SQL syntax error near `Offset INT UNSIGNED`: If a dump was made before MariaDB 10.6 (LinkAhead <
  0.15) and with the SQL backend before 7.0.3, there was a pramater named `Offset`.  With MariaDB
  10.6 however, `OFFSET` became a reserved keyword.  This can be fixed by running  
  `2024-10-02.dump_fix_mariadb_10_6.sh < yourdump.sql > yourdump.fixed.sql`
